$(function() {
    $('#upload_file').submit(function(e) {
        e.preventDefault();
        $.ajaxFileUpload({
            url             :'http://spajalica.app:8000/emails/upload_file', 
            secureuri       :false,
            fileElementId   :'userfile',
            dataType        : 'json',
            data            : {
                'title'             : $('#title').val()
            }
            success : function (data, status)
            {
                $('#files').html('<p>Prep....</p>');
                alert(data.status);
                if(data.status != 'error')
                {
                    $('#files').html('<p>Reloading files...</p>');
                    refresh_files();
                    $('#title').val('');
                }else{
                    $('#files').html(data.msg);
                    refresh_files();
                    $('#title').val('');
                    alert(data.msg);

                }
                alert(data.msg);
            }
        });
        return false;
    });
});