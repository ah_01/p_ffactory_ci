<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "main";
$route['404_override'] = '';
$route['menu'] = 'main/menu';

//$route['login'] = 'login';

$route['change_language/(:any)'] = 'main/change_language';

$route['search'] = 'main/search';
$route['search/page/(:any)'] = 'main/search';
$route['search/page'] = 'main/search';

$route['sendemails'] = 'main/emailsendchron';
$route['sendemails/(:num)'] = 'main/emailsendchron';

$route['contact'] = 'main/contact';
$route['contact/send'] = 'main/contact/send';

// vea add - THIS MUST BE SIMPLIFIED AND CHANGED WITH CACHED ROUTES!!!
require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$urls = $db->get('categories')->result_array();
foreach ($urls as $url) {
    $route[$url['url']] = "content/category";

    $route[$url['url']."/page/(:any)"] = "content/category";
    $route[$url['url']."/page"] = "content/category";

    $route[$url['url']."/(:any)"] = "content/article";
}

/* End of file routes.php */
/* Location: ./application/config/routes.php */