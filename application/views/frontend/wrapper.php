<?php $this->load->view('/frontend/head'); ?>
<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
<?php 
     $is_logged_in = $this->session->userdata('is_logged_in');
     if (isset($is_logged_in) && $is_logged_in == true && isset($articles) && sizeof($articles) <= 1) {
         echo '<div style="position: fixed; top: 30px; right: 50px; padding: 5px 20px; background: #990000; color: #FFFFFF;"><a href="'.base_url(). 'admini/content/edit/'.$articles[0]['id'].'" style="color: #FFF !important;">EDIT</a></div>';
     }

 ?>
<div id="wrapper" class="grid1 clearfix">
  <hr class="top" />
<?php $this->load->view('/frontend/header'); ?>
  <hr class="reset" />

<?php // var_dump($view);

    $this->load->view('frontend/layouts/'.$view['layout']);
?>

<?php $this->load->view('/frontend/footer'); ?>
  <hr class="bottom" />
  <a id="top-link" href="#top">TOP</a>
</div>
<!-- end of #wrapper --> 

<?php $this->load->view('/frontend/scripts'); ?>

</body>
</html>