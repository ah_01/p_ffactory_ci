<?php foreach ($articles as $article) : ?>
<article>
    <h2><a href="<?=base_url().$category['url'].'/'.$article['url']?>"><span class="subject"><?=$article['subject']?></span><?=$article['title']?></a></h2>
    <div class="post-text" style="margin: 10px 0;">
            <?=$article['introtext']?>
    </div>
            <?php
                if (!empty($article['image'])) {
                    $imgdata = @getimagesize(substr_replace(base_url(),"",-1).$article['image']);
                    $width = ($imgdata[0] <= 400) ? '200' : '573';
                    $style = ($imgdata[0] <= 400) ? '' : 'margin-right: 0px; margin-bottom: 0px;';
                    $class = ($imgdata[0] <= 400) ? 'left' : '';
                    echo '<img src="'.$article['image'].'" class="'.$class.'" alt="title" width="'.$width.'" style="'.$style.'" />';
                }
            ?>
    <div class="post-text">
            <?=$article['fulltext']?>
    </div>
    <section>
        <?php
                $gallery = unserialize($article['gallery']);
                // vea add - the way it should not be verified!!!
                if (strlen($gallery[0]) != 0) :
        ?>
        <h5>Gallery: </h5>
            <ul class="post-gallery" id="post-gallery" style="margin-left: 0px;">
            <?php foreach ($gallery as $image) : ?>
                <li><a href="<?=substr_replace(base_url(),"",-1).$image?>" target="_blank"><img src="<?=substr_replace(base_url(),"",-1).$image?>" style="padding: 0px; margin: 0px; border: 0px;" /></a><span>This is an example of the basic theme.</span></li>
            <?php endforeach; ?>
            </ul>
            <div class="clearfix"></div>
            <div style="margin-top: 10px; border-bottom: 1px solid #E9E9E9;"></div>
        <?php endif; ?>
            
        <?php
                $attachments = unserialize($article['attachments']);
                if (strlen($attachments[0]) != 0) :
        ?>
        <h5>Prilozi: </h5>
            <ul class="links">
            <?php foreach ($attachments as $attachment) : ?>
                <?php $name = substr($attachment,(strrpos($attachment, '/')+1)); ?>
                <li><a href="<?=$attachment?>" target="_blank"><img src="/images/icons/filetypes/<?=substr($name, strrpos($name, '.') + 1)?>.png" class="icon4" /><?=$name?></a></li>
            <?php endforeach; ?>
            </ul>
            <div class="clearfix"></div>
        <?php endif; ?>
    </section>
<!--     <div class="post-footer meta document">
        <?=($article['author'] != "") ?  "Piše: ".$article['author']  : "" ; echo '&nbsp;&nbsp;|&nbsp;'; ?>
        Datum: <?=date("F jS, Y",  strtotime($article['date_update'])). '&nbsp;&nbsp;|&nbsp;'; ?>
        <?=($article['source'] != "") ?  'Izvor: '.auto_link($article['source'], 'url', TRUE) : "" ;?>
    </div> -->
    <hr />
</article>
<?php endforeach;   ?>

<?php if (isset($rel_cat)): ?>
 <div class="grid3 frontpage">
        <h4>Faculty members detailed</h4>
            
            <?php foreach ($rel_cat as $link) : ?>
                
                <!-- <div style="font-size: 12px; line-height: 13px; font-weight: bold; font-size: 12px;"><?=character_limiter($link['subject'],40)?></div> -->
                
                <li><a href="<?=base_url().'Faculty/'.$link['url']?>"><?=character_limiter($link['title'],40)?></a></li>
                 <!-- <p><?=character_limiter($link['introtext'],135)?></p> -->
            <?php endforeach; ?><br>
            <a href="<?=base_url() ?>static-page/faculty"> << back to Faculty page</a>
<!--            <a href="<?=base_url().'aktuelnosti'?>" class="archive">(Arhiva...)</a>-->
      </div>

<?php endif; ?>