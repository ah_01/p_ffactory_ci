<?php foreach ($articles as $article) : ?>
<article>
    <h2><a href="<?=base_url().$category['url'].'/'.$article['url']?>"><span class="subject"><?=$article['subject']?></span><?=$article['title']?></a></h2>
        <?=(!empty($article['image'])) ? '<a href="'.base_url().$category['url'].'/'.$article['url'].'">'.'<img src="'.$article['image'].'" class="left" alt="title" width="200" /></a>' : "" ;?>
    <div class="post-text">
        <?=$article['introtext']?>
    </div>
    <section>
    <div class="post-read-more"><a href="<?=base_url().$category['url'].'/'.$article['url']?>">Opširnije...</a></div>
        <div class="post-footer meta document">
            <?=($article['author'] != "") ?  "Piše: ".$article['author']  : "" ; echo '&nbsp;&nbsp;|&nbsp;'; ?>
             Datum: <?=date("F jS, Y",  strtotime($article['date_update'])).'&nbsp;&nbsp;|&nbsp;'; ?>
            <?=($article['source'] != "") ?  'Izvor: <a href="http://'.$article['source'].'" target="_blank">'.$article['source'].'</a>'  : "" ;?>
            <?php
                    $attachments = unserialize($article['attachments']);
                    if (strlen($attachments[0]) != 0)  {
                        echo '&nbsp;|&nbsp;&nbsp;'."has attachment";
                    }
            ?>
            <?php
                    $gallery = unserialize($article['gallery']);
                    if (strlen($gallery[0]) != 0) {
                        echo '&nbsp;|&nbsp;&nbsp;'."has gallery";
                    }
            ?>
        </div>
    </section>
        <hr />
</article>
<?php endforeach;   ?>
