<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link href='http://fonts.googleapis.com/css?family=Headland+One|Judson:700|Cambo|Esteban|Radley' rel='stylesheet' type='text/css'>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>film.factory - Home</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/main.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>css/ff_details.css" />

<link href="<?=base_url(); ?>favicon.ico" rel="shortcut icon"/>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37201295-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

  <!-- SCRIPTS -->
            <script src="/scripts/modernizr-1.5.min.js"></script>
            <script src="/scripts/jquery-1.7.2.min.js"></script>
            <script src="/scripts/plugins.js"></script>
            <script src="/scripts/jquery.pikachoose.js"></script>
            <script src="/scripts/default.js"></script>
