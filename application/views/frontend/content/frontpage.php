<?php if (isset($frontpage_slider)) {  ?>
    <div id="gallery">
        <ul>
            <?php $images = unserialize($frontpage_slider); ?>
            <?php foreach ($images as $image) : ?>
                <li><img src="<?=$image?>" alt="gallery" width="870" height="300" /></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php }; ?>
<!--  <p class="tagline">Udruženje „Spajalica“ je nevladina, neprofitna i nepolitička organizacija koja je osnovana s ciljem promoviranja, razvoja i unapređenja obrazovanja u Bosni i Hercegovini. Temelje Udruženja su, augusta 2008. godine, postavili nekadašnji i sadašnji studenti Univerziteta u Sarajevu.</p>-->


<div id="main" class="clearfix" style="margin-top: 10px;">




  </div>

      <div class="grid3 frontpage">
        <h2>Latest News</h2>

            <?php foreach ($news as $link) : ?>

                <!-- <div style="font-size: 12px; line-height: 13px; font-weight: bold; font-size: 12px;"><?=character_limiter($link['subject'],40)?></div> -->

                <a href="<?=base_url().'news/'.$link['url']?>"><?=character_limiter($link['title'],40)?></a><br />
                 <p><?=character_limiter($link['introtext'],135)?></p>
            <?php endforeach; ?>
<!--            <a href="<?=base_url().'aktuelnosti'?>" class="archive">(Arhiva...)</a>-->
      </div>

    <div class="grid3 frontpage reset">

    </div>

  </div>