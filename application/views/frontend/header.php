<!-- InstanceBeginEditable name="head" -->
<title>film.factory - Home</title>
<!-- InstanceEndEditable -->
</head>


<body>
<div class="container">
<!-- <p id="language"> <a href="#">Bosanski</a> <a href="#">English</a></p> -->
  <div class="header"><a href="#"><img src="<?=base_url(); ?>img/logo.jpg" alt="film.factory logo"
  	name="film.factory logo" width="863" height="213" id="filmfactory_logo"
  	style="background: #000; display:block; margin: 0 auto;" /></a>


  	<!-- Menu:START -->
<div id="menu_wrap">
    <ul>
        <li><a href="<?=base_url('/static-page/welcome'); ?>">Home</a>
        <ul>
            <!-- <li class="button"><a href="<?=base_url('/static-page/welcome')?>">Welcome to film.factory</a></li> -->
            <li class="button"><a href="<?=base_url('/static-page/about-filmfactory')?>">About film.factory</a></li>
            <li  class="button"><a href="<?=base_url()?>static-page/the-university-and-foundation">The University and Foundation</a></li>
            <!-- <li class="button"><a href="http://ssst.edu.ba" target="_blank">SSST</a></li> -->
         </ul>
         </li>
        <li><a href="<?=base_url()?>static-page/programme">Programmes</a>
            <ul>
                <li class="button">
                <a href="<?=base_url()?>static-page/ba-programme">Undergraduate - BA Programme</a></li>
                <li class="button">
                <a href="<?=base_url()?>static-page/mfa-pprogramme">Graduate - MFA Programme</a></li>
                <li class="button">
                <a href="<?=base_url()?>static-page/dla-programme">Doctoral level - DLA Programme</a></li></ul>
                </li>


        <li><a href="<?=base_url()?>static-page/faculty">Faculty</a></li>
        <li><a href="<?=base_url()?>static-page/admissions">Admissions</a>
        <ul>
            <a href="<?=base_url()?>static-page/application-form-requirements">Application forms</a>
            <a href="<?=base_url()?>static-page/tuition-fees">Tuition Fees</a>
        </ul>

            </li>
        <li><a href="<?=base_url()?>static-page/academic-calendar">Academic Calendar</a></li>
        <li><a href="<?=base_url()?>static-page/film-factory-lab">film.factory Lab</a></li>
        <li><a href="<?=base_url()?>static-page/students">Students</a>


        </li>

        <li><a href="#">Media</a>

            <ul>
                    <li>
                    <a href="<?=base_url()?>static-page/media-about-us">Media about us</a></li>
                    <li><a href="https://www.facebook.com/filmfactory.sarajevo" target="_blank">film.factory Facebook</a> </li>
                    <!-- <li><a href="https://twitter.com/FilmfactoryInfo" target="_blank">film.factory Tweeter</a> </li> -->
            </ul>


        </li>


        <li><a href="<?=base_url()?>static-page/partners">Partners</a></li>
        <li><a href="<?=base_url()?>static-page/contacts">Contacts</a></li>
    </ul>
</div>



<!-- Menu:END -->
    <!-- end .header --></div>

<div class="content">

<H3><?=$page_description; ?></H3>


