<?php
    if (isset($title) && isset($page_description)) {
        echo "<h1>$title<span>$page_description</span></h1><hr />";
    }
?>
<div id="main" class="clearfix">
    
    <div class="grid2">
    <?php
        if (isset($view['mainbar']) && is_array($view['mainbar'])) {
            foreach ($view['mainbar'] as $mainbar) {
                $this->load->view('frontend/content/mainbar/'.$mainbar);
            }
        } elseif (isset($view['mainbar'])) {
                $this->load->view('frontend/content/mainbar/'.$view['mainbar']);
        }
    ?>
    </div>
    
    <div class="grid3 reset">
    <?php
        if (isset($view['sidebar']) && is_array($view['sidebar'])) {
            foreach ($view['sidebar'] as $sidebar) {
                $this->load->view('frontend/content/sidebar/'.$sidebar);
            }
        } elseif (isset($view['sidebar'])) {
            $this->load->view('frontend/content/sidebar/'.$view['sidebar']);
        }
    ?>
    </div>
</div>