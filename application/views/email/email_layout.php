<?php $this->load->view('email/components/header'); ?>

<body>

	<h1>Email management</h1>

	<section>
	
	<ul class="nav nav-tabs" role="tablist">
		<li <?php if(!$this->uri->segment(2)) echo 'class="active"'; ?>><a href="<?=site_url('emails') ?>">Emails</a></li>
		<li <?php if($this->uri->segment(2) == 'edit') echo 'class="active"'; ?>><a href="<?=site_url('emails/edit') ?>">Email messages</a></li>
		<li <?php if($this->uri->segment(2) == 'recipients') echo 'class="active"'; ?>><a href="<?=site_url('emails/recipients') ?>">Recipients</a></li>
		<li <?php if($this->uri->segment(2) == 'tasks') echo 'class="active"'; ?>><a href="<?=site_url('emails/tasks') ?>">Tasks</a></li>
	</ul>


	</section>
	
<?php $this->load->view("email/$subview"); ?>


</body>
<?php $this->load->view('email/components/footer'); ?>
