        <div id="results"></div>
<div class="container">
<br>


<h4>List of emails in DB</h4>
<table id="myTable" class="stripe hover row-border order-column">
    <thead>
        <tr>

            <th>Id</th>
            <th>Email</th>
            <th>Status</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Group</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
    </thead>
    <tbody>
<?php foreach ($results as $email) : ?>

        <tr>
            <td><?=$email->id; ?></td>
            <td><?=$email->email; ?></td>
            <td><?= $email->status ? "TRUE" : "FALSE"; ?></td>
            <td><?=$email->created; ?></td>
            <td><?=$email->updated; ?></td>
            <td id="submit">
            <?php 

               
                echo form_input('mgroup', $email->mgroup);
                echo form_submit(array(
                    'name'=>'submit',
                    'value'=>'s',
                    'class'=>'s',
                    'data1'=>$email->id,
                    ));
               

            ?>
</td>
            <td align="center"><?=anchor('emails/recipients/' . $email->id, '<span class="glyphicon glyphicon-pencil">'); ?></td>

            <td align="center"><?=anchor( 'emails/rdelete/' . $email->id, '<span class="glyphicon glyphicon-minus">',array(
                          'class'       => 'confirm-link',
                          'data-link'   => 'delete/' . $email->id,
                          )); ?></td>

        </tr>
    <?php endforeach; ?>
       
    </tbody>
</table>
<?php //echo $links; ?>
 </div>