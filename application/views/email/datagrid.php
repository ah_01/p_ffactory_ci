
<?php
        $this->Datagrid->hidePkCol(true);
        if($error = $this->session->flashdata('form_error')){
            echo "<font color=red>$error</font>";
        }
        echo form_open('dg_test/proc',array('class'=>'dg_form'));
        echo $this->Datagrid->generate();
        echo Datagrid::createButton('delete','Delete');
        echo form_close();
