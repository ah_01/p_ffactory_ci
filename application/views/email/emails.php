
<div class="container">
<h3>Please paste email addresses into form</h3>

<?php
	// var_dump($this->session->flashdata('error'));
	echo alert_m($this->session->flashdata('error'));


?>

<div class="col-sm-12">

<!-- ************************************************** -->

				<?php
				if (isset($email_messages)) {
					echo '<div class="alert alert-info" role="alert">' . $email_messages . '</div>';
				}
				
				// Check for ID in adress - if there is one we need to update  article not add new
				if ($id = $this->uri->segment(3)) {
					$id = '/' . $id;
				}else{
					$id = '';
				}


				echo form_open('emails/add_message' . $id);

				echo form_label('Subject');
				echo "<br>";


				echo form_input(array(
											'name'        => 'subject',
								            'id'          => 'subject',
								            'value'       => $subject,
								            'maxlength'   => '100',
								            'size'        => '50',
								            'style'       => 'width:50%',
              ));
				echo "<hr>";

				echo form_label('Body');
				echo "<br>";


				echo form_textarea(array(
					'id'	=> 'body',
					'name'	=> 'body',
					'class'	=> 'body',
					'value'	=> $body,

					));
				echo "<hr>";
				// echo "<br>";
				echo form_submit(array(
					'class'=>'btn btn-primary',
					'name'=>'submit',
					'value'=>'Submit',

					));


				echo form_close();

		 ?>

		<table class="table table-striped">
			<thead>
				<tr>

				    <th>Id</th>
				    <th>Subject</th>
				    <th>Subject</th>
				    <th>Status</th>
				    <th>---</th>
				    <th>---</th>
				    <th>---</th>
				    <th>Send</th>
				    <th>Edit</span></th>
				    <th>Delete</th>

				</tr>
				</thead>
				<tbody>


<?php 
if (is_array($letters)) :
foreach ($letters as $email) : ?>

        <tr>
		    <td><?=$email->id; ?></td>
		    <td><?=anchor('emails/edit/'. $email->id, $email->subject); ?></td>
		    <td><?=$email->status; ?></td>
		    <td>---</td>
		    <td>---</td>
		    <td>---</td>
		    <td>---</td>
		    <td> <span class="glyphicon glyphicon-envelope"> </td>
		    <td><?=anchor('emails/edit/' . $email->id, '<span class="glyphicon glyphicon-pencil">'); ?></td>
		    <td><?=anchor( '#', '<span class="glyphicon glyphicon-minus">',array(
		    			  'class'		=> 'confirm-link',
		    			  'data-link'	=> 'delete/' . $email->id,
		    			  )); ?></td>
	    </tr>

    <?php endforeach;
    	 else: ?>
	        <tr>
		    <th>No data to display</th>
		    <th> --- </th>
		    <th> --- </th>
		    <th>---</th>
		    <th>---</th>
		    <th>---</th>
		    <th>---</th>
		    <th>---</th>
		    <th> --- </th>
		    <th> --- </th>
	    </tr>
    <?php endif; ?>

		</tbody>




		</table>



<!-- ///////////////////////				MODAL			///////////////////////// -->

<!-- ////////////////////////////////////// -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete record</h4>
      </div>
      <div class="modal-body">
        Do you really wish to delete this record permanently?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" id="btnYes" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>


<!-- ////////////////////////////////////// -->

 </div>
 <script>
		$('#myModal').on('show', function() {
		var link = $(this).data('link'),
		    agreeBtn = $(this).find('.agree');
		})

		$('.confirm-link').on('click', function(e) {
		    e.preventDefault();

		    var link = $(this).data('link');
		    $('#myModal').data('link',link).modal('show');
		});

		$('#btnYes').click(function() {
		    // handle redirect here
		  	var link = $('#myModal').data('link');
		  	location.href = link;
		    $('#myModal').modal('hide');
		});



 	$('#demo').collapse('hide');
 	$('#demo1').collapse('hide');
 </script>