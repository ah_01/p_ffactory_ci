<html>
<head>
	<meta charset="UTF-8">
	<title>Email management</title>
	<script src="http://code.jquery.com/jquery-2.1.1.js"></script>

	<!-- Bootstrap -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	
	<!-- Data tables -->
	<link href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

	<script src="//cdn.ckeditor.com/4.4.3/full/ckeditor.js"></script>

	<script src="<?php echo base_url(); ?>scripts/datagrid.js"></script>
	<script src="<?php echo base_url(); ?>scripts/jquery.sieve.min.js"></script>
		
</head>
