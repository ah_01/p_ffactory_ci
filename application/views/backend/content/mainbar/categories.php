<label for="">Edit/Create Category: </label>

<?php echo form_open('admini/categories/save'); ?>

<div class="row">
<?php 
        if (validation_errors() != "") {
               echo form_label('Errors',"");
               echo validation_errors();
        }
?>
</div>

<?php
    if (isset($category['id'])) {
                echo form_hidden('id',$category['id']);
            } else {
                echo form_hidden('id',set_value('id')); 
        }
?>
    
<div class="row">
    <table class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;">
                <?php
                    echo form_label("Name: ","name");
                    if (isset($category['name'])) { $val = $category['name']; } else { $val = set_value('name'); }
                    echo form_input('name',$val,'class="box4" id=""');
                ?>
            </td>
            <td>
                <?php
                    echo form_label("URL: <small>(Only URL format!!!)</small>","url");
                    if (isset($category['url'])) { $val = $category['url']; } else { $val = set_value('url'); }
                    echo form_input('url',$val,'class="box4" id=""');
                ?>
            </td>
            <td style="padding-right: 0px;">
                <?php
                    echo form_label("Description: ","description");
                    if (isset($category['description'])) { $val = $category['description']; } else { $val = set_value('description'); }
                    echo form_input('description',$val,'class="box4" id=""');
                ?>
            </td>
        </tr>
    </table>
</div>

<div class="row">
    <table  class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;"  valign="top">
                <?php
                    echo form_label("Sidebar content: <small>(Press Ctrl for multi-select)</small>","sidebar_content");
                    if (isset($category['sidebar_content'])) { $tmp = explode(',',$category['sidebar_content']); $val = $tmp; } else { $val = set_value('sidebar_content'); }
                    $result = array();
                    foreach ($sidebar_content as $value) { $result[$value['id']] = $value['title']; }
                    echo form_multiselect('sidebar_content[]', $result, $val,'class="box5" style="height: 230px;"');
                ?>
            </td>
            <td style="padding-right: 0px;" valign="top">
                <?php
                    echo form_label("Articles per page: ","articles_per_page");
                    if (isset($category['articles_per_page'])) { $val = $category['articles_per_page']; } else { $val = (set_value('articles_per_page') != '') ? set_value('articles_per_page') : 0; }
                    echo form_input('articles_per_page',$val,'class="box4" id="" style="width: 50px;"');
                    echo form_button('decrease','-',' class="decrease" id="decrease"');
                    echo form_button('increase','+',' class="increase" id="increase"');
                ?>
                <br /><br />
                <?php
                    echo form_label("Number of linx in sidebar: ","number_of_linx");
                    if (isset($category['number_of_linx'])) { $val = $category['number_of_linx']; } else { $val =  (set_value('number_of_linx') != '') ? set_value('number_of_linx') : 0; }
                    echo form_input('number_of_linx',$val,'class="box4" id="" style="width: 50px;"');
                    echo form_button('decrease','-',' class="decrease" id="decrease"');
                    echo form_button('increase','+',' class="increase" id="increase"');
                ?>
                <br /><br />
                <?php
                    echo form_label("Layout: ","layout"); if (isset($category['layout'])) { $val = $category['layout']; } else { $val = set_value('layout'); }
                    $result = array();
                    foreach ($layouts as $value) { $result[preg_replace("/\\.[^.\\s]{3,4}$/", "", $value)] = preg_replace("/\\.[^.\\s]{3,4}$/", "", $value); }
                    echo form_dropdown('layout', $result, $val,'class="box5"');
                ?>
                <br />
                <?php
                    echo form_label("Articles layout: ","article_layout");
                    if (isset($content['article_layout'])) { $val = $content['article_layout']; } else { $val = set_value('article_layout'); }
                    $result = array();
                    foreach ($article_layouts as $value) { $result[preg_replace("/\\.[^.\\s]{3,4}$/", "", $value)] = preg_replace("/\\.[^.\\s]{3,4}$/", "", $value); }
                    echo form_dropdown('article_layout', $result, $val,'class="box5"');
                ?>
            </td>

        </tr>
    </table>
</div>

<div class="row">
<?php
    echo form_label("Tags:  <small>(Nakon svakog taga otkucati zarez.)</small>","tags");
    if (isset($category['tags'])) { $val = $category['tags']; } else { $val = set_value('tags'); }
    echo form_input('tags',$val,'class="box" id="tags" class="tags"');
?>
</div>

<div class="row">
    <table  class="cols3table" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="padding-left: 0px;">
            <?php # image select
                    echo form_label("Icon <small>(Click on field to browse server)</small>","icon");
                    if (isset($category['icon'])) { $val = $category['icon']; } else { $val = set_value('icon'); }
                    echo form_input('icon',$val,' style="cursor: pointer; width: 285px" class="box4" readonly="readonly" onclick="openKCFinderIframe(this, \'images\',\'\')"  ');
                    echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev"');
            ?>
            <div id="kcfinder_iframe_div" style="display: none; position: absolute; z-index: 10; width: 670px; height: 400px; background: #e0dfde; border: 2px solid #30A4B1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; padding: 1px;"></div>
            </td>
            <td style="padding-right: 0px;">
            <?php # image select
                    echo form_label("Image <small>(Click on field to browse server)</small>","image");
                    if (isset($category['image'])) { $val = $category['image']; } else { $val = set_value('image'); }
                    echo form_input('image',$val,' style="cursor: pointer; width: 285px" class="box4" readonly="readonly" onclick="openKCFinderIframe(this, \'images\',\'\')"  ');
                    echo form_button('reset-prev','RESET',' class="reset-prev" id="reset-prev"');
            ?>
            <div id="kcfinder_iframe_div" style="display: none; position: absolute; z-index: 10; width: 670px; height: 400px; background: #e0dfde; border: 2px solid #30A4B1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; padding: 1px;"></div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <?php if (isset($category['icon'])) { $val = $category['icon']; } else { $val = set_value('icon'); } ?>
                <?php if ($val != '') echo '<img src="'.$val.'" class="preview-icon" />'; ?>
            </td>
            <td valign="top">
                <?php if (isset($category['image'])) { $val = $category['image']; } else { $val = set_value('image'); } ?>
                <?php if ($val != '') echo '<img src="'.$val.'" class="preview-image" />'; ?>
            </td>
        </tr>
    </table>
</div>

<div class="row" style="text-align: right;">
    <?php
            echo form_button('cancel','cancel','class="cancel" onclick="location.href=\'http://www.spaja-lica.com/admini/categories\'" ');
            echo form_submit('submit', 'Submit Form', 'class="submit"');
    ?>
</div>
<?php echo form_close(); ?>

<div class="row">
    <label for="">Categories list: </label>
          <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="width: 100%;">
                    <thead style="background-color: #efefef; font-weight: bolder; ">
                        <td width="20"><strong>ICO</strong></td>
                        <td width=""><strong>NAME</strong></td>
                        <td width="100"><strong>URL</strong></td>
                        <td width="20"><strong>APP</strong></td>
                        <td width="20"><strong>NL</strong></td>
                        <td width="80"><strong>LAYOUT</strong></td>
                        <td width="70"><strong>A.LAYOUT</strong></td>
                        <td width="60"><strong>EDIT/DELL</strong></td>
                    </thead>
                    <tbody>
            <?php foreach ($categories as $item) : ?>
                    <tr>
                        <td><small><img src="<?=($item['icon'] != '') ? base_url($item['icon']) : base_url('/images/icons/weather_clouds.png')?>" class="icon4" /></small></td>
                        <td><a href="/admini/categories/edit/<?=$item['id']?>"><?=$item['name']?></a></td>
                        <td><?=$item['url']?></td>
                        <td><?=$item['articles_per_page']?></td>
                        <td><?=$item['number_of_linx']?></td>
                        <td><?=$item['layout']?></td>
                        <td><?=$item['article_layout']?></td>
                        <td>
                                <a href="/admini/categories/edit/<?=$item['id']?>" class=""><img src="<?=base_url()?>images/icons/page_edit.png"  alt="Edit" title="Edit" class="icon2" /></a>
                                </a>
                                <a href="#" url="/admini/categories/delete/<?=$item['id']?>" class="confirm" action="[delete]" message="<?='<strong>'.htmlspecialchars($item['name'].'<br /><span style="color: #900; text-transform:uppercase;">Ovu operaciju može izvršiti samo glavni administrator!</span>').'</strong>'?>"><img src="<?=base_url()?>images/icons/page_delete.png"  alt="Delete" title="Delete" class="icon2" /></a>
                                <a href="<?=base_url().$item['url']?>" class="" target="_blank"><img src="<?=base_url()?>images/icons/popup.png"  alt="Preview" title="Preview" class="icon2" /></a>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
</div>