<div class="row">
<?php
$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
//$br = $this->db->where('update_date','0000-00-00 00:00:00')->get('content')->num_rows();
//echo $br;
?>
PRETRAGA
</div>
<div class="row">
    <label for="">Content: </label>
        <table width="850" border="0" cellspacing="0" cellpadding="10" class="table_main" style="width: 100%;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <!-- <td width="50"><strong>DATE</strong></td> -->
                <td width="10"><strong>LN</strong></td>
                <td width="55"><strong>CAT</strong></td>
                <td width="460"><strong>TITLE</strong></td>
                <td width="95"><strong>OPTIONS</strong></td>
            </thead>
            <tbody>
    <?php foreach ($content_list as $item) : ?>
            <tr style="<?php if ($item['date_update'] != '0000-00-00 00:00:00') echo "background: #CCFF99;"; ?>">
                <!-- <td><small><?=date('d.m.Y', strtotime($item['date_update']))?></small></td> -->
                <td><small style="color: <?php if ($item['language'] == 'english') echo '#CC3333'; else echo '#0099FF'; ?>"><?=$item['language']?></small></td>
                <td><small style=""><?=$categories[$item['category_id']]['name']?></small></td>
                <td><a href="<?=base_url()?>admini/content/edit/<?=$item['id']?>"><?=$item['title']?></a></td>
                <td>
                        <a href="<?=base_url()?>admini/content/edit/<?=$item['id']?>" class=""><img src="<?=base_url()?>images/icons/page_edit.png"  alt="Edit" title="Edit" class="icon2" /></a>
                        <a href="#" url="/admini/content/status/<?=$item['id']?>" class="confirm" action="[change status]" message="<?='<strong>'.htmlspecialchars($item['title']).'</strong>'?>">
                            <?php if ($item['status'] == 1) $icon = 'page_green.png'; else $icon = 'page_red.png'; ?>
                            <img src="<?=base_url()?>images/icons/<?=$icon?>"  alt="Status" title="Status" class="icon2" />
                        </a>
                        <a href="#" url="/admini/content/delete/<?=$item['id']?>" class="confirm" action="[delete]" message="<?='<strong>'.htmlspecialchars($item['title']).'</strong>'?>"><img src="<?=base_url()?>images/icons/page_delete.png"  alt="Delete" title="Delete" class="icon2" /></a>
                        <a href="<?=base_url().$categories[$item['category_id']]['url'].'/'.$item['id']?>" class="" target="_blank"><img src="<?=base_url()?>images/icons/popup.png"  alt="Preview" title="Preview" class="icon2" /></a>
                        <a href="<?=base_url().$categories[$item['category_id']]['url'].'/'.$item['url']?>" class="" target="_blank"><img src="<?=base_url()?>images/icons/popup.png"  alt="Preview" title="Preview" class="icon2" /></a>
                </td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <?php echo $this->pagination->create_links(); ?>
</div>