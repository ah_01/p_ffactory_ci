<?php
//var_dump($prijave);
$opstine = $this->config->item('opstine');
$fakulteti = $this->config->item('fakulteti');
$roditelji = Array('1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
$trajanje_studija = Array('3'=>'3 godine', '4'=>'4 godine','5'=>'5 godina','6'=>'6 godina');
$godina_studija = Array('1'=>'1. godina','2'=>'2. godina','3'=>'3. godina','4'=>'4. godina','5'=>'5. godina','6'=>'6. godina');
$neda = Array('0'=>'NE','1'=>'DA');
$status = Array('-1' => 'Aplikacija poništena','0' => 'Aplikacija bez statusa!','1' => 'Email poslan','2' => 'Kliknutno na link u Emailu!','3' => 'SMS poslan!','4' => 'SMS verifikovan!','5' => 'PONIŠTENO!');
$sms_status = $this->config->item('sms_status');
?>
<div class="row">
    [Ukupno prijava: <?=sizeof($prijave)?>]  | <a href="http://www.spaja-lica.com/admini/konkursi/rezultati" target="_blenk">Preliminarni rezultati sa filtriranim sadrzajem! <img src="/images/icons/popup.png" alt="Preview" title="Preview" class="icon2"></a>
</div>
<div class="row">
    <label for="">Spisak studenata: [SIMPLE LIST]</label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 10px; width: 100%;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th style="width: 140px;">IME (ime roditelja) PREZIME</th>
                <th>EMAIL</th>
                <th>MOBITEL</th>
                <th>UNSA</th>
                <th>FAKULTET</th>
                <th>STATUS</th>
                <th>STIP.</th>
                <th style="width: 105px;">DATUM</th>
                <th>BOD.</th>
                <th>PDF</th>
            </thead>
            <tbody>
    <?php foreach ($prijave as $prijava) : ?>
                <?php
                    $ostalo = unserialize($prijava['ostalo']);
                    $session = unserialize($prijava['session']);
                    $bodovanje = unserialize($prijava['bodovanje']);
                ?>
                <tr>
                    <td><span style="color: #900;"><?=$prijava['ime'].' ('.$prijava['ime_roditelja'].') '.$prijava['prezime']?></span></td>
                    <td><a href="mailto:<?=$prijava['email']?>" target="_blank"><?=$prijava['email']?></td>
                    <td>+<?=$prijava['mobitel']?></td>
                    <td <?php echo ($ostalo['univerzitet_sarajevo'] == 0) ? 'style="background: #990000; color: #fff; text-align: center; "' : 'style="text-align: center; "' ;?>>
                        <?=$neda[$ostalo['univerzitet_sarajevo']]?>
                    </td>
                    <td><small><?=character_limiter($fakulteti[$prijava['fakultet']],20)?></small></td>
                    <td><?=character_limiter($status[$prijava['status']],10)?></td>
                    <td <?php echo ($ostalo['prima_stipendiju'] == 0) ? 'style="text-align: center;"' : 'style="background: #990000; color: #fff; text-align: center;"' ;?>>
                            <?=$neda[$ostalo['prima_stipendiju']]?>
                    </td>
                    <td><?=date('d.m.Y H:i:m',strtotime($prijava['datetime']))?></td>
                    <td><?=$prijava['bodovi']?></td>
                    <td><a href="<?=base_url()?>admini/konkursi/topdf/<?=$prijava['id']?>" target="_blank"><img src="/images/icons/filetypes/pdf.png" border="0" style="border: none; vertical-align: middle;" /></a></td>
                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>
<small>Zbirna lista svih prijavljenih studenata! Ovo je samo pojednostavljeni pregled i kao takav nije relevantan za javno predstavljanje te podlježe komisijskim ispravkama.</small>
