<?php
//var_dump($ukupno_aplikacija);//var_dump($ukupno_aktiviranih_aplikacija);//var_dump($ukupno_ponistenih_aplikacija);
//var_dump($ukupno_ne_aktivirane);//var_dump($ukupno_ne_zadovoljava_kriterije);//var_dump($ukupno_zadovoljava_kriterije);
//var_dump($studenata_po_fakultetima);//var_dump($studenata_po_opstinama);//var_dump($studenata_po_trajanju_studija);
//var_dump($studenata_po_godini_studija);//var_dump($roditelji);//var_dump($rvi_u_porodici);//var_dump($top_prosjek_brucosi);//var_dump($top_prosjek_vise_godine);

$opstine = $this->config->item('opstine');
$fakulteti = $this->config->item('fakulteti');
$roditelji_opis = Array('1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
$trajanje_studija = Array('3'=>'3 godine', '4'=>'4 godine','5'=>'5 godina','6'=>'6 godina');
$godina_studija = Array('1'=>'1. godina','2'=>'2. godina','3'=>'3. godina','4'=>'4. godina','5'=>'5. godina','6'=>'6. godina');
$neda = Array('0'=>'NE','1'=>'DA');
$gender_opis = Array('0'=>'ŽENSKO', '1'=>'MUŠKO');
$status = Array('-1' => 'Aplikacija poništena','0' => 'Aplikacija bez statusa!','1' => 'Email poslan','2' => 'Kliknutno na link u Emailu!','3' => 'SMS poslan!','4' => 'SMS verifikovan!','5' => 'PONIŠTENO!');
$sms_status = $this->config->item('sms_status');
?>

<?php 
    $ukupno_zadovoljava_kriterije = 0;
    $poslanih_sms = 0;
    $isporucenih_sms = 0;
    $komentara = 0;
    $male = 0;
    $female = 0;
    
    foreach ($aplikacije as $aplikacija) {
            $ostalo = unserialize($aplikacija['ostalo']);
            $session = unserialize($aplikacija['session']);
            $bodovanje = unserialize($aplikacija['bodovanje']);
            
            if ($ostalo['univerzitet_sarajevo'] == 1 && $ostalo['redovan'] == 1 && $ostalo['prima_stipendiju'] == 0 && $ostalo['stalno_prebivaliste'] == 1 && $aplikacija['status'] == 4) { $ukupno_zadovoljava_kriterije++; }
            if ($aplikacija['komentar'] != '') { $komentara++; }
            if ($aplikacija['sms_api_id'] != '') { $poslanih_sms++; }
            if ($aplikacija['sms_status'] == '004') { $isporucenih_sms++; }
            
            //if ($aplikacija['gender'] == '0') { $female++; } else { $male++; }
    }
?>

<div class="row">
<div style="float: left;">
<label for="">PROSTA STATISTIKA PRIJAVA</label>
    <table width="" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 12px; text-align: center; width: 220px;">
        <tbody>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">AKTIVIRANIH PRIJAVA</td><td style="text-align: right;"><?=$ukupno_aktiviranih_aplikacija?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">NEAKTIVIRANIH PRIJAVA</td><td style="text-align: right;"><?=$ukupno_ne_aktivirane?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">PONIŠTENIH PRIJAVA</td><td style="text-align: right;"><?=$ukupno_ponistenih_aplikacija?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">POSLANIH SMS-ova</td><td style="text-align: right;"><?=$poslanih_sms?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">ISPORUČENIH SMS-ova</td><td style="text-align: right;"><?=$isporucenih_sms?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">KOMENTARA</td><td style="text-align: right;"><?=$komentara?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">NEZADOVOLJAVA KRITERIJE</td><td style="text-align: right;"><?=$ukupno_aplikacija-$ukupno_zadovoljava_kriterije?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">ZADOVOLJAVA KRITERIJE</td><td style="text-align: right;"><?=$ukupno_zadovoljava_kriterije?></td></tr>
                <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; ">UKUPNO</td><td style="text-align: right;"><?=$ukupno_aplikacija?></td></tr>
        </tbody>
    </table>
</div>
    <div style="float: left; margin-left: 30px;">
    <label for="">BROJ PRIJAVA PO DANIMA</label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 12px; text-align: center; width: 180px;">
            <tbody>
                <?php $data_for_chart = ''; ?>
                        <?php foreach ($aplikacije_po_danima as $aplikacija) : ?>
                                <tr>
                                    <td scope="row" style="background-color: #efefef; font-weight: bolder; "><?=$aplikacija['datum']?></td>
                                    <td style="text-align: right;"><?=$aplikacija['aplikacija']?></td></tr>
                            <?php $data_for_chart .= '[\''.$aplikacija['datum'].'\','.$aplikacija['aplikacija'].'],'; ?>
                        <?php endforeach; ?>
            </tbody>
        </table>            
    </div>
    <div style="float: left; margin-left: 30px;">
    <label for="">BROJ PRIJAVA PO DANIMA</label>
            <div id="aplikacije-intenzitet-vizuelno" style="height:220px; width: 500px; "></div>
             <script class="code" type="text/javascript">
                    $(document).ready(function(){
                      var line1= <?='['.$data_for_chart.']'?>;
                      var plot2 = $.jqplot('aplikacije-intenzitet-vizuelno', [line1], {
                          title:'',
                          shadow: false,
                          axes:{
                            xaxis:{
                              renderer:$.jqplot.DateAxisRenderer, 
                              min: '2012-11-6',
                              max: '2012-11-16',
                              pad: 1.1,
                              tickOptions:{formatString:'%b %#d %Y.'},
                             }
                          },
                          seriesDefaults:[{
                                  lineWidth: 4,
                                  markerOptions: {show: false,style:'filledCircle'},
                                  shadow: false,
                                  shadowOffset: 1,    // offset from the line of the shadow,
                                  shadowDepth: 3,     // Number of strokes to make when drawing shadow.  Each stroke
                                  pointLabels: {show: true}
                              }]
                      });
                    });            
             </script>
    </div>
<div style="clear: both"></div>
</div>

<div class="row">
    <div style="float: left;">
        <label for="">STUDENATA NA GODINI STUDIJA</label>
            <table width="" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 220px;">
                <thead style="background-color: #efefef; font-weight: bolder; ">
                    <th>FAKULTET</th>
                    <th style="text-align: center;">BROJ STUDENATA</th>
                </thead>
                <tbody> <?php asort($studenata_po_godini_studija); $data_for_chart = ''; ?>
                    <?php foreach ($studenata_po_godini_studija as $item) : ?>
                        <tr>
                            <td><?=$godina_studija[$item['godina_studija']]?></td>
                            <td style="text-align: center;"><?=$item['studenata']?></td>
                        </tr>
                        <?php $data_for_chart .= '[\''.$godina_studija[$item['godina_studija']].'\','.$item['studenata'].'],'; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div id="data_for_chart" style="height:300px;width:220px; "></div>
             <script class="code" type="text/javascript">
                    $(document).ready(function(){
                        var data_for_chart = <?='['.$data_for_chart.']'?>;
                        var data_for_chart = jQuery.jqplot ('data_for_chart', [data_for_chart], 
                            { 
                              seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer, 
                                rendererOptions: {
                                  padding: 5, 
                                  showDataLabels: true,
                                  sliceMargin: 4,
                                  shadowOffset: 0,    // offset from the bar edge to stroke the shadow.
                                  shadowDepth: 0     // nuber of strokes to make for the shadow.
                                }
                              }, 
                              legend: { show:true, location: 's', rendererOptions: {numberRows: 3} },
                              seriesColors: [ "#9AD007", "#FFA500", "#FFD700", "#7AB4F5", "#8B008B", "#D11F67"], 
                              grid:{borderWidth:0, shadow:false} 
                            }
                          );
                    });
            </script>
    </div>
    <div style="float: left; margin-left: 20px;">
        <label for="">STUDENATA PO TRAJANJU STUDIJA</label>
        <table width="" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 220px;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th>FAKULTET</th>
                <th style="text-align: center;">BROJ STUDENATA</th>
            </thead>
            <tbody> <?php asort($studenata_po_trajanju_studija); $data_for_chart = ''; ?>
                <?php foreach ($studenata_po_trajanju_studija as $item) : ?>
                    <tr>
                        <td><?=$trajanje_studija[$item['trajanje_studija']]?></td>
                        <td style="text-align: center;"><?=$item['studenata']?></td>
                    </tr>
                    <?php $data_for_chart .= '[\''.$trajanje_studija[$item['trajanje_studija']].'\','.$item['studenata'].'],'; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div id="data_for_chart2" style="height:300px;width:220px; "></div>
        <script class="code" type="text/javascript">
               $(document).ready(function(){
                   var data_for_chart = <?='['.$data_for_chart.']'?>;
                   var data_for_chart = jQuery.jqplot ('data_for_chart2', [data_for_chart], 
                       { 
                         seriesDefaults: {
                           // Make this a pie chart.
                           renderer: jQuery.jqplot.PieRenderer, 
                           rendererOptions: {
                             padding: 5,
                             showDataLabels: true,
                             sliceMargin: 4,
                             shadowOffset: 0,    // offset from the bar edge to stroke the shadow.
                             shadowDepth: 0     // nuber of strokes to make for the shadow.
                           }
                         }, 
                         legend: { show:true, location: 's', rendererOptions: {numberRows: 3} },
                         seriesColors: [ "#9AD007", "#FFA500", "#FFD700", "#7AB4F5", "#8B008B", "#D11F67"], 
                         grid:{borderWidth:0, shadow:false} 
                       }
                     );
               });
       </script>
    </div>
    <div style="float: left; margin-left: 20px;">
        <label for="">STATUS RODITELJSTVA</label>
        <table width="" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 230px;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th>STATUS</th>
                <th style="text-align: center;">BROJ STUDENATA</th>
            </thead>
            <tbody> <?php asort($roditelji); $data_for_chart = ''; ?>
                <?php foreach ($roditelji as $item) : ?>
                    <tr>
                        <td><?=$roditelji_opis[$item['roditelji']]?></td>
                        <td style="text-align: center;"><?=$item['studenata']?></td>
                    </tr>
                    <?php $data_for_chart .= '[\''.$roditelji_opis[$item['roditelji']].'\','.$item['studenata'].'],'; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div id="data_for_chart3" style="height:300px;width:220px; "></div>
        <script class="code" type="text/javascript">
               $(document).ready(function(){
                   var data_for_chart = <?='['.$data_for_chart.']'?>;
                   var data_for_chart = jQuery.jqplot ('data_for_chart3', [data_for_chart], 
                       { 
                         seriesDefaults: {
                           // Make this a pie chart.
                           renderer: jQuery.jqplot.PieRenderer, 
                           rendererOptions: {
                             padding: 5,
                             showDataLabels: true,
                             sliceMargin: 4,
                             shadowOffset: 0,    // offset from the bar edge to stroke the shadow.
                             shadowDepth: 0     // nuber of strokes to make for the shadow.
                           }
                         }, 
                         legend: { show:true, location: 's', rendererOptions: {numberRows: 3} },
                         seriesColors: [ "#9AD007", "#FFA500", "#FFD700", "#7AB4F5", "#8B008B", "#D11F67"], 
                         grid:{borderWidth:0, shadow:false} 
                       }
                     );
               });
       </script>
    </div>
    <div style="float: left; margin-left: 20px;">
        <label for="">PORODICA SA RVI</label>
        <table width="" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 220px;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th>PORODICA RVI</th>
                <th style="text-align: center;">BROJ STUDENATA</th>
            </thead>
            <tbody> <?php asort($rvi_u_porodici); $data_for_chart = ''; ?>
                <?php foreach ($rvi_u_porodici as $item) : ?>
                    <tr>
                        <td><?=$neda[$item['porodica_rvi']]?></td>
                        <td style="text-align: center;"><?=$item['studenata']?></td>
                    </tr>
                    <?php $data_for_chart .= '[\''.$neda[$item['porodica_rvi']].'\','.$item['studenata'].'],'; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div id="data_for_chart4" style="height:300px;width:220px; "></div>
        <script class="code" type="text/javascript">
               $(document).ready(function(){
                   var data_for_chart = <?='['.$data_for_chart.']'?>;
                   var data_for_chart = jQuery.jqplot ('data_for_chart4', [data_for_chart], 
                       { 
                         seriesDefaults: {
                           // Make this a pie chart.
                           renderer: jQuery.jqplot.PieRenderer, 
                           rendererOptions: {
                             padding: 5,
                             showDataLabels: true,
                             sliceMargin: 4,
                             shadowOffset: 0,    // offset from the bar edge to stroke the shadow.
                             shadowDepth: 0     // nuber of strokes to make for the shadow.
                           }
                         }, 
                         legend: { show:true, location: 's', rendererOptions: {numberRows: 3} },
                         seriesColors: [ "#9AD007", "#FFA500", "#FFD700", "#7AB4F5", "#8B008B", "#D11F67"], 
                         grid:{borderWidth:0, shadow:false} 
                       }
                     );
               });
       </script>
    </div>
</div>

<div class="row">
    <div style="float: left;">

    </div>
    <div style="float: left; margin-left: 30px;">

    </div>
    <div style="clear: both"></div>
</div>

<div class="row">
<div style="float: left;">
        <label for="">STUDENATA PO OPŠTINAMA/OPĆINAMA</label>
    <table width="" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 450px;">
        <thead style="background-color: #efefef; font-weight: bolder; ">
            <th style="width: 320px;">FAKULTET</th>
            <th>BROJ STUDENATA</th>
        </thead>
        <tbody>
                <?php foreach ($studenata_po_opstinama as $item) : ?>
                <tr>
                    <td><?=$opstine[$item['opstina']]['naziv']?></td>
                    <td style="text-align: center;"><?=$item['studenata']?></td>
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div style="float: right;">
    <label for="">GENDER PROPORCIJA</label>
    <table width="500" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 480px;">
        <tbody> <?php asort($gender); $data_for_chart = ''; ?>
                <?php foreach ($gender as $item) : ?>
                    <tr><td scope="row" style="background-color: #efefef; font-weight: bolder; "><?=$gender_opis[$item['gender']]?></td><td style="text-align: center;"><?=$item['studenata']?></td></tr>
                <?php $data_for_chart .= '[\''.$gender_opis[$item['gender']].'\','.$item['studenata'].'],'; ?>
                <?php endforeach; ?>
        </tbody>
    </table>
    <div id="data_for_chart5" style="height:400px;width:480px; "></div>
    <script class="code" type="text/javascript">
               $(document).ready(function(){
                   var data_for_chart = <?='['.$data_for_chart.']'?>;;
                   var data_for_chart = jQuery.jqplot ('data_for_chart5', [data_for_chart], 
                       { 
                         seriesDefaults: {
                           // Make this a pie chart.
                           renderer: jQuery.jqplot.PieRenderer, 
                           rendererOptions: {
                             padding: 5,
                             showDataLabels: true,
                             sliceMargin: 4,
                             shadowOffset: 0,    // offset from the bar edge to stroke the shadow.
                             shadowDepth: 0     // nuber of strokes to make for the shadow.
                           }
                         }, 
                         legend: { show:true, location: 'e', rendererOptions: {numberRows: 3} },
                         seriesColors: [ "#D11F67", "#7AB4F5", "#FFD700", "#9AD007", "#FFA500", "#8B008B"], 
                         grid:{borderWidth:0, shadow:false} 
                       }
                     );
               });
       </script>
       <label for="">STUDENATA PO FAKULTETIMA</label>
        <table width="500" border="0" cellspacing="0" cellpadding="3" class=""  style="font-size: 12px; width: 480px; margin-top: 20px;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th>FAKULTET</th>
                <th>BROJ STUDENATA</th>
            </thead>
            <tbody>
                <?php foreach ($studenata_po_fakultetima as $item) : ?>
                <tr>
                    <td><?=character_limiter($fakulteti[$item['fakultet']],40)?></td>
                    <td style="text-align: center;"><?=$item['studenata']?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<div style="clear: both"></div>
</div>

<div class="row">
    <label for="">Spisak 20 studenata sa najvećim prosjekom: [VIŠE GODINE]</label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 10px; width: 100%;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th style="width: 140px;">IME (ime roditelja) PREZIME</th>
                <th>EMAIL</th>
                <th>MOBITEL</th>
                <th>UNSA</th>
                <th>FAKULTET</th>
                <th>STATUS</th>
                <th>STIP.</th>
                <th>BOD.</th>
                <th>PROSJEK</th>
                <th>PDF</th>
            </thead>
            <tbody>
    <?php foreach ($top_prosjek_vise_godine as $prijava) : ?>
                <?php
                    $ostalo = unserialize($prijava['ostalo']);
                    $session = unserialize($prijava['session']);
                    $bodovanje = unserialize($prijava['bodovanje']);
                ?>
                <tr>
                    <td><span style="color: #900;"><?=$prijava['ime'].' ('.$prijava['ime_roditelja'].') '.$prijava['prezime']?></span></td>
                    <td><a href="mailto:<?=$prijava['email']?>" target="_blank"><?=$prijava['email']?></td>
                    <td>+<?=$prijava['mobitel']?></td>
                    <td <?php echo ($ostalo['univerzitet_sarajevo'] == 0) ? 'style="background: #990000; color: #fff; text-align: center; "' : 'style="text-align: center; "' ;?>>
                        <?=$neda[$ostalo['univerzitet_sarajevo']]?>
                    </td>
                    <td><small><?=character_limiter($fakulteti[$prijava['fakultet']],30)?></small></td>
                    <td><?=character_limiter($status[$prijava['status']],20)?></td>
                    <td <?php echo ($ostalo['prima_stipendiju'] == 0) ? 'style="text-align: center;"' : 'style="background: #990000; color: #fff; text-align: center;"' ;?>>
                            <?=$neda[$ostalo['prima_stipendiju']]?>
                    </td>
                    <td><?=$prijava['bodovi']?></td>
                    <td><?=$prijava['prosjek_ocjena']?></td>
                    <td><a href="<?=base_url()?>admini/konkursi/topdf/<?=$prijava['id']?>" target="_blank"><img src="/images/icons/filetypes/pdf.png" border="0" style="border: none; vertical-align: middle;" /></a></td>
                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <label for="">Spisak 20 studenata sa najvećim prosjekom: [BRUCOŠI]</label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 10px; width: 100%;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th style="width: 140px;">IME (ime roditelja) PREZIME</th>
                <th>EMAIL</th>
                <th>MOBITEL</th>
                <th>UNSA</th>
                <th>FAKULTET</th>
                <th>STATUS</th>
                <th>STIP.</th>
                <th>BOD.</th>
                <th>PROSJEK</th>
                <th>PDF</th>
            </thead>
            <tbody>
    <?php foreach ($top_prosjek_brucosi as $prijava) : ?>
                <?php
                    $ostalo = unserialize($prijava['ostalo']);
                    $session = unserialize($prijava['session']);
                    $bodovanje = unserialize($prijava['bodovanje']);
                ?>
                <tr>
                    <td><span style="color: #900;"><?=$prijava['ime'].' ('.$prijava['ime_roditelja'].') '.$prijava['prezime']?></span></td>
                    <td><a href="mailto:<?=$prijava['email']?>" target="_blank"><?=$prijava['email']?></td>
                    <td>+<?=$prijava['mobitel']?></td>
                    <td <?php echo ($ostalo['univerzitet_sarajevo'] == 0) ? 'style="background: #990000; color: #fff; text-align: center; "' : 'style="text-align: center; "' ;?>>
                        <?=$neda[$ostalo['univerzitet_sarajevo']]?>
                    </td>
                    <td><small><?=character_limiter($fakulteti[$prijava['fakultet']],30)?></small></td>
                    <td><?=character_limiter($status[$prijava['status']],20)?></td>
                    <td <?php echo ($ostalo['prima_stipendiju'] == 0) ? 'style="text-align: center;"' : 'style="background: #990000; color: #fff; text-align: center;"' ;?>>
                            <?=$neda[$ostalo['prima_stipendiju']]?>
                    </td>
                    <td><?=$prijava['bodovi']?></td>
                    <td><?=$prijava['prosjek_ocjena']?></td>
                    <td><a href="<?=base_url()?>admini/konkursi/topdf/<?=$prijava['id']?>" target="_blank"><img src="/images/icons/filetypes/pdf.png" border="0" style="border: none; vertical-align: middle;" /></a></td>
                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <label for="">SVI KOMENTARI</label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main"  style="font-size: 10px; width: 100%;">
            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th style="width: 140px;">IME (ime roditelja) PREZIME</th>
                <th>EMAIL</th>
                <th>MOBITEL</th>
                <th>FAKULTET</th>
                <th>STATUS</th>
                <th>BOD.</th>
                <th>PROSJEK</th>
                <th>PDF</th>
            </thead>
            <tbody>
    <?php foreach ($aplikacije as $prijava) : ?>
                <?php
                    $ostalo = unserialize($prijava['ostalo']);
                    $session = unserialize($prijava['session']);
                    $bodovanje = unserialize($prijava['bodovanje']);
                    if ($prijava['komentar'] == '') continue;
                ?>
                <tr>
                    <td><span style="color: #900;"><?=$prijava['ime'].' ('.$prijava['ime_roditelja'].') '.$prijava['prezime']?></span></td>
                    <td><a href="mailto:<?=$prijava['email']?>" target="_blank"><?=$prijava['email']?></td>
                    <td>+<?=$prijava['mobitel']?></td>
                    <td><small><?=character_limiter($fakulteti[$prijava['fakultet']],30)?></small></td>
                    <td><?=character_limiter($status[$prijava['status']],20)?></td>
                    <td><?=$prijava['bodovi']?></td>
                    <td><?=$prijava['prosjek_ocjena']?></td>
                    <td><a href="<?=base_url()?>admini/konkursi/topdf/<?=$prijava['id']?>" target="_blank"><img src="/images/icons/filetypes/pdf.png" border="0" style="border: none; vertical-align: middle;" /></a></td>
                </tr>
                <tr>
                    <td colspan="8" style="font-size: 14px;"><?=$prijava['komentar']?></td>
                </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>

