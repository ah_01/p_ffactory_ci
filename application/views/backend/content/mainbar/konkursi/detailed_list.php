<?php
//var_dump($prijave);
$opstine = $this->config->item('opstine');
$fakulteti = $this->config->item('fakulteti');
$roditelji = Array('1'=>'Imam oba roditelja','2'=>'Bez jednog roditelja','3'=>'Bez oba roditelja');
$trajanje_studija = Array('3'=>'3 godine', '4'=>'4 godine','5'=>'5 godina','6'=>'6 godina');
$godina_studija = Array('1'=>'1. godina','2'=>'2. godina','3'=>'3. godina','4'=>'4. godina','5'=>'5. godina','6'=>'6. godina');
$neda = Array('0'=>'NE','1'=>'DA');
$status = Array('-1' => 'Aplikacija poništena','0' => 'Aplikacija bez statusa!','1' => 'Email poslan','2' => 'Kliknutno na link u Emailu!','3' => 'SMS poslan!','4' => 'SMS verifikovan!','5' => 'PONIŠTENO!');
$sms_status = $this->config->item('sms_status');
?>

<!--[if IE]><style>.rotate_text { writing-mode: tb-rl; filter: flipH() flipV(); } </style><![endif]--> <!--[if !IE]><!-->
<style> .rotate_text { -moz-transform:rotate(-90deg); -moz-transform-origin: top left; -webkit-transform: rotate(-90deg); -webkit-transform-origin: top left; -o-transform: rotate(-90deg); -o-transform-origin:  top left; position:relative; top:14px; }</style>
<!--<![endif]-->
<style>.rotated_cell { height:45px; vertical-align:bottom; }</style>

<div class="row">
    [Ukupno prijava: <?=sizeof($prijave)?>]  | <a href="http://www.spaja-lica.com/admini/konkursi/rezultati" target="_blenk">Preliminarni rezultati sa filtriranim sadrzajem! <img src="/images/icons/popup.png" alt="Preview" title="Preview" class="icon2"></a>
</div>
<div class="row">
    <label for="">Spisak studenata: </label>
        <table width="850" border="0" cellspacing="0" cellpadding="3" class="table_main" style=" width: 100%;" >
<!--            <thead style="background-color: #efefef; font-weight: bolder; ">
                <th style="width: 280px;">Lični podatci</th>
                <th>Ostalo</th>
            </thead>-->
            <tbody>
    <?php foreach ($prijave as $prijava) : ?>
                <?php
                    $ostalo = unserialize($prijava['ostalo']);
                    $session = unserialize($prijava['session']);
                    $bodovanje = unserialize($prijava['bodovanje']);
                ?>
            <tr style="background-color: #efefef; font-weight: bolder; ">
                <td style="width: 280px;">Lični podatci</td>
                <td>Ostalo</td>
            </tr>
                <tr>
                        <td style="vertical-align: top;">
                            <strong>Ime: </strong><span style="color: #900;"><?=$prijava['ime'].' ('.$prijava['ime_roditelja'].') '.$prijava['prezime']?></span><br />
                            <strong>Mjesto rođenja: </strong><?=$prijava['mjesto_rodjenja']?><br />
                            <strong>Datum rođenja: </strong><?=date('d.m.Y',strtotime($prijava['datum_rodjenja']))?><br />
                            <strong>Email: </strong><a href="mailto:<?=$prijava['email']?>" target="_blank"><?=$prijava['email']?></a><br />
                            <strong>Telefon: </strong>+<?=$prijava['telefon']?><br />
                            <strong>Mobitel: </strong>+<?=$prijava['mobitel']?><br />
                            <strong>Adresa: </strong><br /><?=$prijava['adresa']?><br />
                            <strong>Opcina/Grad/Distrikt: </strong><br /><?=$opstine[$prijava['opstina']]['naziv']?><br />
                            <strong>Fakultet: </strong><br /><?=$fakulteti[$prijava['fakultet']]?><br />
                            <hr />
                            <div>
                                <?php
                                    echo form_open('admini/konkursi/status/'.$prijava['id'],'style="display: inline;"',array('id'=>$prijava['id']));
                                    echo form_dropdown('status', $status,$prijava['status'],'style="display: inline;"');
                                    echo form_submit('submit','SNIMI','class="submit" style="display: inline; width:60px;"');
                                    echo form_close();
                                ?>
                                <a href="<?=base_url()?>admini/konkursi/topdf/<?=$prijava['id']?>" target="_blank">[ PDF<img src="/images/icons/popup.png" width="10" alt="Preview" title="Preview" class="icon2"> ]</a> 
                            </div>
                        </td>
                        <td style="vertical-align: top;">
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="width: 100%;">
                                <thead style="">
                                        <th>Trajanje studija</th>
                                        <th>Godina studija</th>
                                        <th>Prosjek ocjena</th>
                                        <th>Ukupna primanja</th>
                                        <th>Broj članova dom.</th>
                                        <th>Roditelji</th>
                                        <th>Porodica RVI</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=$trajanje_studija[$prijava['trajanje_studija']]?></td>
                                        <td><?=$godina_studija[$prijava['godina_studija']]?></td>
                                        <td style="background: #6414DB; color: #fff;"><?=$prijava['prosjek_ocjena']?></td>
                                        <td style="background: #9C6EE0; color: #fff;"><?=$prijava['ukupna_primanja']?></td>
                                        <td style="background: #C0B8CC; color: #fff;"><?=$prijava['ukupno_clanova']?></td>
                                        <td><?=$roditelji[$prijava['roditelji']]?></td>
                                        <td><?=$neda[$prijava['porodica_rvi']]?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="width: 100%;">
                                <thead style="">
                                        <th>Student UNSA</th>
                                        <th>Redovan</th>
                                        <th>Uvjerenje red. šk.</th>
                                        <th>Uvjerenje pol. isp.</th>
                                        <th>Izjava ne prim. st.</th>
                                        <th>Stalno prebiv. BiH</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=$neda[$ostalo['univerzitet_sarajevo']]?></td>
                                        <td><?=$neda[$ostalo['redovan']]?></td>
                                        <td><?=$neda[$ostalo['uvjerenje_redovno_skolovanje']]?></td>
                                        <td><?=$neda[$ostalo['uvjerenje_polozeni_ispiti']]?></td>
                                        <td><?=$neda[$ostalo['izjava_o_neprimanju_stipendije']]?></td>
                                        <td><?=$neda[$ostalo['stalno_prebivaliste']]?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="width: 100%;">
                                <thead style="">
                                        <th>Fotografije</th>
                                        <th>Samo jedna pri.</th>
                                        <th>Uvjerenje domać.</th>
                                        <th>Dokaz o prim.</th>
                                        <th>Dokaz RVI</th>
                                        <th>Prima stipendiju</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?=$neda[$ostalo['fotografije']]?></td>
                                        <td><?=$neda[$ostalo['samo_jedna_prijava']]?></td>
                                        <td><?=$neda[$ostalo['uvjerenje_domacinstvo']]?></td>
                                        <td><?=$neda[$ostalo['dokaz_o_primanjima']]?></td>
                                        <td><?=$neda[$ostalo['dokaz_o_rvi']]?></td>
                                        <td <?php echo ($ostalo['prima_stipendiju'] == 0) ? 'style="background: #9AD007; color: #fff;"' : 'style="background: #990000; color: #fff;"' ;?>>
                                                <?=$neda[$ostalo['prima_stipendiju']]?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="width: 100%;">
                                <thead style="">
                                        <th>Status</th>
                                        <th>SMS kod</th>
                                        <th>SMS status</th>
                                        <th>IP</th>
                                        <th>Vrijeme Apliciranja</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: left; background: #D11F67; color: #fff;"><?=$status[$prijava['status']]?></td>
                                        <td style="text-align: left;"><?=$prijava['sms_kod']?></td>
                                        <td style="text-align: left; background: #7AB4F5; color: #fff;"><?=$sms_status[$prijava['sms_status']]['short']?></td>
                                        <td style="text-align: left;"><?=$prijava['ip']?></td>
                                        <td style="text-align: left;"><?=date('d.m.Y H:i:m',strtotime($prijava['datetime']))?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="background: #DDD; width: 100%;">
                                <thead style="">
                                        <th>BODOVANJE</th>
                                        <th>Prosjek</th>
                                        <th>God. St.</th>
                                        <th>Ukupno Šk.</th>
                                        <th>Mat. St.</th>
                                        <th>Roditelji</th>
                                        <th>Porodica RVI</th>
                                        <th>Status čl. obit.</th>
                                        <th>UKUPNO</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><?=number_format($bodovanje['prosjek_ocjena'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['godina_studija'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['ukupno_skolovanje'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['materijalno_stanje'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['roditelji_bodova'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['porodica_rvi_bodova'], 2, '.', '')?></td>
                                        <td><?=number_format($bodovanje['status_uzih_clanova_porodice'], 2, '.', '')?></td>
                                        <!--<td style="background: #9AD007; color: #fff;"><?=number_format($bodovanje['ukupno'], 2, '.', '')?></td>-->
                                        <td style="background: gold; font-weight: bold;"><?=number_format($prijava['bodovi'], 2, '.', '')?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table border="0" cellspacing="0" cellpadding="3" class="table_sub" style="width: 100%;">
                                <tr>
                                    <td style="border: none; text-align: left;"><strong>KOMENTAR:</strong> <?=$prijava['komentar']?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>
<small>Zbirna lista svih prijavljenih studenata! Ovo je samo pojednostavljeni pregled i kao takav nije relevantan za javno predstavljanje te podlježe komisijskim ispravkama.</small>

