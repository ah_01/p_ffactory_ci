<?php // if (validation_errors() != "") echo '<label>Errors:</label>'.validation_errors().''; ?>

<label>TODO</label>
<div style="overflow: hidden;">
    <div id="accordion">
        <div>
            <h3><a href="#">Image editor</a></h3>
            <div>Svaki put kada se odabere slika, treba ostaviti mogucnost da se ucita i "Image editor" tako da je moguce sliku cropati, smanjiti je na odgovarajucu velicinu i slicno.</div>
        </div>
        <div>
            <h3><a href="#">HELP</a></h3>
            <div>
                    Napisati Help za koristenje CMS-a
            </div>
        </div>
        <div>
            <h3><a href="#">CATCH management</a></h3>
            <div>
                    Implementirati catch management, ajax request -> brisanje -> reply o uspjesnosti i statusu.
            </div>
        </div>
        <div>
            <h3><a href="#">Session data</a></h3>
            <div>
                    <?php var_dump($this->session->all_userdata()); ?>
            </div>
        </div>
    </div>
</div>
<br />
<label>LAST EDITED</label>
<div>
    <?php
        $last_in_session = $this->session->userdata('last_edit');
        if(!isset($last_in_session) || $last_in_session == '') {
            echo "none";
        } else {
            // var_dump($last_in_session);
            foreach (array_reverse($last_in_session) as $key => $value) {
                echo '<a href="/admini/content/edit/'.$key.'">'.$value.'</a><hr style="background: none; border-top: 1px solid #ccc;">';
                //echo '<a href="'.base_url().$ar['category_url'].'/'.$key.'" class="" target="_blank"><img src="/images/icons/popup.png"  alt="Preview" title="Preview" class="icon2" /></a>';
            }
        }
    ?>
</div>
