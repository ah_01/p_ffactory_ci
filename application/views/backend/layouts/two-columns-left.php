<div class="right-2columns">
    
    <div class="mainbar">
    <?php
        if (isset($view['mainbar']) && is_array($view['mainbar'])) {
            foreach ($view['mainbar'] as $mainbar) {
                $this->load->view('/backend/content/mainbar/'.$mainbar);
            }
        } elseif (isset($view['mainbar'])) {
                $this->load->view('/backend/content/mainbar/'.$view['mainbar']);
        }
    ?>
    </div>
    
    <div class="right-sidebar">
    <?php
        if (isset($view['sidebar']) && is_array($view['sidebar'])) {
            foreach ($view['sidebar'] as $sidebar) {
                $this->load->view('/backend/content/sidebar/'.$sidebar);
            }
        } elseif (isset($view['sidebar'])) {
            $this->load->view('/backend/content/sidebar/'.$view['sidebar']);
        }
    ?>
    </div>
</div>