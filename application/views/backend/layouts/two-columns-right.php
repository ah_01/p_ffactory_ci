<div class="content-header">
        <h2><?=$title?></h2>
        <p>
        <?php
            $day = Array('1'=> 'Ponedjeljak','2'=>'Utorak','3'=>'Srijeda','4'=>'Četvrtak','5'=>'Petak','6'=>'Subota','7'=>'Nedjelja');
            $month = Array('1'=> 'Januar','2'=>'Februar','3'=>'Mart','4'=>'April','5'=>'Maj','6'=>'Juni','7'=>'Juli','8'=>'Avgust','9'=>'Septembar','10'=>'Oktobar','11'=>'Novembar','12'=>'Decembar');
            echo $day[date('N',time())].', '.date('d. ',time()).$month[date('n',time())].date(' Y.',time());
        ?>
        </p>
</div>
<div class="line"></div>
<div class="two-columns-right">
    <div class="mainbar">
    <?php
        if (isset($view['mainbar']) && is_array($view['mainbar'])) {
            foreach ($view['mainbar'] as $mainbar) {
                $this->load->view('/backend/content/mainbar/'.$mainbar);
            }
        } elseif (isset($view['mainbar'])) {
                $this->load->view('/backend/content/mainbar/'.$view['mainbar']);
        }
    ?>
    </div>
    <div class="right-sidebar">
    <?php
        if (isset($view['sidebar']) && is_array($view['sidebar'])) {
            foreach ($view['sidebar'] as $sidebar) {
                $this->load->view('/backend/content/sidebar/'.$sidebar);
            }
        } elseif (isset($view['sidebar'])) {
            $this->load->view('/backend/content/sidebar/'.$view['sidebar']);
        }
    ?>
    </div>
    <div class="clear"></div>
</div>