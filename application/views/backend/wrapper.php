<?php $this->load->view('/backend/head'); ?>
<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<div class="wrapper">
    <?php $this->load->view('/backend/header'); ?>
    <div class="content">
        <?php $this->load->view('/backend/layouts/'.$view['layout']); ?>
    </div>   
    <?php $this->load->view('/backend/footer'); ?>
</body>
</html>