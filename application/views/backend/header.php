<header>
    <h1 class="logo"><?=$this->session->userdata('username')?>@ssst.edu.ba</h1>
    <p class="txt_right">Logged in as <strong><?=$this->session->userdata('name').' '.$this->session->userdata('surname')?></strong> <span class="v_line"> | </span>
        <a href="mailto:<?=$this->session->userdata('email')?>" target="_blank"><?=$this->session->userdata('email')?></a> <span class="v_line"> | </span> <a href="/login/logout"> Logout</a> <span class="v_line"> | </span> <a href="<?=base_url()?>" target="_blank"><?=base_url()?><img src="/images/icons/popup2.png" class="icon3"></a>
    </p><br>
    <!-- Navigation -->
    <div class="navbar navigation round-segment">
                <ul class="menu">
                            <li><a href="<?=base_url()?>admini" class="round-segment">DASHBOARD</a></li>
                            <li><a href="<?=base_url()?>admini/settings" class="round-segment">SETTINGS</a></li>
                            <li>
                                <a href="#" class="round-segment">CONTENT</a>
                                <ul class="menu-sub">
                                    <li style="padding: 0px; background: #30A4B1; border-bottom: 1px dotted #FFF;"></li>
                                    <li><a href="<?=base_url()?>admini/content/form"><img src="<?=base_url()?>images/icons/newspaper_add.png" class="icon" />NEW ARTICLE</a></li>
                                    <li><a href="<?=base_url()?>admini/content"><img src="<?=base_url()?>images/icons/newspaper.png" class="icon" />LIST ARTICLES</a></li>
                                    <li style="padding: 0px; background: #30A4B1; border-bottom: 1px dotted #FFF;"></li>
                                    <li><a href="<?=base_url()?>admini/categories"><img src="<?=base_url()?>images/icons/asterisk_yellow.png" class="icon" />CATEGORIES</a></li>
                                    <li style="padding: 0px; background: #30A4B1; border-bottom: 1px dotted #FFF;"></li>
                                    <li><a href="<?=base_url()?>admini/browser"><img src="<?=base_url()?>images/icons/database_table.png" class="icon" />FILE BROWSER</a></li>
                                </ul>
                            </li>
                    </ul>
                    <div id="searchform">
                            <form method="post" action="<?=base_url()?>admini/content/search">
                            <input type="text" value="Search only title and text fields. Min 3 chars." class="search_box round-segment" name="term" onclick="this.value='';"  />
                            <input type="submit" class="search_btn round-segment" value="SEARCH" />
                            </form>
                    </div>
    </div>
    <div class="clear"></div>
  </header>