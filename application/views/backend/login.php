<? //header("Content-Type: text/html; charset=UTF-8"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>login@<?=substr(base_url(),7,-1)?></title>
        <script language="javascript" type="text/javascript" src="http://www.stipendije.ba/scripts/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="http://www.stipendije.ba/scripts/jquery.corner.js"></script>
        <style type="text/css">
            body {
                margin: 0;
                padding: 0;
                font-family: arial;
            }
            #login_form, #contact_form {
                background: #f0f0f0;
                width: 410px;
                border: 1px solid #9AD007;
                margin: 100px auto 0;
                padding: 1em;
                -moz-border-radius: 3px;
                text-align: center;
            }
            h1,h2,h3,h4,h5 {
                margin-top: 0;
                font-family: arial black, arial;
                text-align: center;
            }
            input[type=text], input[type=password], textarea {
                display: block;
                margin: 0 0 1em 0;
                width: 385px;
                border: 1px solid #9AD007;
                -moz-border-radius: 1px;
                -webkit-border-radius: 1px;
                padding: 1em;
            }
            input[type=submit], form a {
                border: none;
                margin-right: 1em;
                padding: 6px;
                text-decoration: none;
                font-size: 12px;
                -moz-border-radius: 4px;
                background: #9AD007;
                color: white;
                box-shadow: 0 1px 0 white;
                -moz-box-shadow: 0 1px 0 white;
                -webkit-box-shadow: 0 1px 0 white;

            }
            input[type=submit]:hover, form a:hover {
                background: #99cc33;
                cursor: pointer;
            }
            /* Validation error messages */
            .error {
                color: #393939;
                font-size: 15px;
            }
            fieldset {
                width: 300px;
                margin: auto;
                margin-bottom: 2em;
                display: block;
            }
            /* FOR FUN */
            h1 {
                text-shadow: 0 1px 0 white;
            }
            h1 a{
                color: #9AD007;
                text-decoration: none;
            }
            /* Not necessary. Just the "tutorial created by" stuff at the bottom */
            #tutInfo {
                background: #e3e3e3;
                border-top: 1px solid white;
                position: absolute;
                bottom: 0;
                width: 100%;
                padding: .7em .7em .7em 2em;
            }
        </style>
        <script type="text/javascript">
            $(function(){
                    $('#login_form').corner("round 5px");
                    $(':input').corner("round 5px");
                    $('[name|="username"]').focus();
                    $(':submit').corner("round 5px");
            });
        </script>
    </head>
    <body>
        <div id="login_form">
            <h1>ADMINISTRACIJA<br /><a href="<?=base_url()?>"><?=substr(base_url(),7,-1)?></a></h1>
            <?php
            echo form_open('login/validate');
            echo form_input('username', '');
            echo form_password('password', '');
            echo form_submit('submit', 'Login');
            //echo anchor('login/signup', 'Create Account');
            echo form_close();
            ?>
        </div>
    </body>
</html>