<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sarajevo School of Science and Technology</title>

    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|&subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|&subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>

    <!-- STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu|Titillium+Web' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/bootstrap.min.css?v=1">
    <link rel="stylesheet" href="/css/font-awesome.min.css?v=1">
    <link rel="stylesheet" href="/css/style.css?v=1">
    <link rel="stylesheet"href="/css/gallery.css?v=1">

    <!-- SCRIPTS -->
    <script src="/scripts/modernizr-1.5.min.js"></script>
    <script type="text/javascript" src="/scripts/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/scripts/bootstrap.min.js"></script>
    <script src="/scripts/plugins.js"></script>
    <script src="/scripts/jquery.pikachoose.js"></script>
    <script src="/scripts/default.js"></script>
    <script src="/scripts/mp3.js"></script>


    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="SSST,Sarajevo School of Science and Technology,Sarajevo,Bih, univerzitet u sarajevu,Sarajevska Škola za nauku i thnologiju, Sarajevska skola za nauku i tehnologiju,bih univerzitet,sarajevo, sarajevska,skola,univerzitet, tehnologija," />
    <meta name="description" content="SSST Sarajevo School of Science and Technology, Sarajevska Skola za nauku i tehnologiju" />
    <link href="<?=base_url('/favicon.ico')?>" rel="shortcut icon"/>

</head>
