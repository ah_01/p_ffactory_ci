<?php $this->load->view('/frontend/head'); ?>
<body>
<?php
     $is_logged_in = $this->session->userdata('is_logged_in');
     if (isset($is_logged_in) && $is_logged_in == true && isset($articles) && sizeof($articles) <= 1) {
         echo '<div style="position: fixed; top: 30px; right: 50px; padding: 5px 20px; background: #990000; color: #FFFFFF;"><a href="'.base_url(). 'admini/content/edit/'.$articles[0]['id'].'" style="color: #FFF !important;">EDIT</a></div>';
     }
         //echo '<div style="text-decoration: blink; position: absolute; top: 30px; left: 50px; padding: 5px 20px; background: #990000; color: #FFFFFF;">construction going on, please be patient</div>';
?>
<!-- start of #wrapper --> 
<div id="wrapper" class="container">
<?php $this->load->view('/frontend/header'); ?>

<?php $this->load->view('frontend/layouts/'.$view['layout']); ?>

<?php $this->load->view('/frontend/footer'); ?>
</div>
<!-- end of #wrapper --> 
<?php $this->load->view('/frontend/scripts'); ?>
</body>
</html>
