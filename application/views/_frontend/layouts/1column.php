<?php
    // if (isset($page_title) && isset($page_description)) {
    //     echo "<h1>$page_title<span>$page_description</span></h1><hr />";
    // }
?>

<?php
 if (isset($view['subheader']) && is_array($view['subheader'])) {
     foreach ($view['subheader'] as $subheader) {
         $this->load->view('frontend/content/subheader/'.$subheader);
     }
 } elseif (isset($view['subheader'])) {
         $this->load->view('frontend/content/subheader/'.$view['subheader']);
 }
?>

<?php
    if (isset($view['content']) && is_array($view['content'])) {
        foreach ($view['content'] as $content) {
            $this->load->view('frontend/content/'.$content);
        }
    } elseif (isset($view['content'])) {
            $this->load->view('frontend/content/'.$view['content']);
    }
?>
