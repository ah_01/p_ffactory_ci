<?php // if (isset($page_title) && isset($page_description)) { //echo "<h1>$page_title<span>$page_description</span></h1><hr />"; } ?>


<div class="row ssst-row">
<?php
 if (isset($view['subheader']) && is_array($view['subheader'])) {
     foreach ($view['subheader'] as $subheader) {
         $this->load->view('frontend/content/subheader/'.$subheader);
     }
 } elseif (isset($view['subheader'])) {
         $this->load->view('frontend/content/subheader/'.$view['subheader']);
 }
?>
</div>

<div class="row ssst-row">
   <div class="col-md-4 ssst-sidebar">
      <?php
         if (isset($view['sidebar']) && is_array($view['sidebar'])) {
             foreach ($view['sidebar'] as $sidebar) {
                 $this->load->view('frontend/content/sidebar/'.$sidebar);
             }
         } elseif (isset($view['sidebar'])) {
             $this->load->view('frontend/content/sidebar/'.$view['sidebar']);
         }
         ?>
   </div>
   <div class="col-md-12 pad ssst-content">
      <?php
         if (isset($view['mainbar']) && is_array($view['mainbar'])) {
             foreach ($view['mainbar'] as $mainbar) {
                 $this->load->view('frontend/content/mainbar/'.$mainbar);
             }
         } elseif (isset($view['mainbar'])) {
                 $this->load->view('frontend/content/mainbar/'.$view['mainbar']);
         }
         ?>
   </div>
</div>