<?php $uri=$this->uri->segment(2); $language = $this->session->userdata('language'); ?>

<?php if ($language == 'bosnian') :?>

<div class="col-xs-16 col-md-6 ssst-no-padding-left" style="">
  <img src="<?=base_url($media_library[0]["image"])?>" alt="" style="width: 100%;" height="100%">
</div>
<div class="col-xs-16 pad col-md-10 ssst-no-padding-right ssst-red ssst-sub-row">
  <small><?=$this->lang->line('media_library')?></small>
  <h3><a href="<?=base_url($categories[$media_library[0]["category_id"]]['url'].'/'.$media_library[0]["url"])?>"><?=word_limiter($media_library[0]["title"], 6)?></a></h3>
  <p class="adjust hidden-xs hidden-sm">
    <?php
    if ($media_library[0]["introtext"] == "")
      echo word_limiter(strip_tags($media_library[0]["fulltext"]),31);
    else
      echo word_limiter(strip_tags($media_library[0]["introtext"]),31);
    ?>
  </p>
</div>

<?php else : ?>

<div class="col-xs-16 col-md-6 ssst-no-padding-left" style="">
  <img src="<?=base_url($media_library[0]["image"])?>" alt="" style="width: 100%;" height="100%">
</div>
<div class="col-xs-16 pad col-md-10 ssst-no-padding-right ssst-red ssst-sub-row">
  <small><?=$this->lang->line('media_library')?></small>
  <h3><a href="<?=base_url($categories[$media_library[0]["category_id"]]['url'].'/'.$media_library[0]["url"])?>"><?=word_limiter($media_library[0]["title"], 6)?></a></h3>
  <p class="adjust hidden-xs hidden-sm">
    <?php
    if ($media_library[0]["introtext"] == "")
      echo word_limiter(strip_tags($media_library[0]["fulltext"]),31);
    else
      echo word_limiter(strip_tags($media_library[0]["introtext"]),31);
    ?>
  </p>
</div>

<?php endif; ?>