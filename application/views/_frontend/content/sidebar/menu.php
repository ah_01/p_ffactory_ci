<?php
   $cat=$this->uri->segment(1);
   $uri=$this->uri->segment(2);
   $language = $this->session->userdata('language');
   ?>
<?php if ($language == 'bosnian') : ?>
<?php
   $ar = array('upoznajte-ssst', 'poruka-rektora', 'ssst-kampus', 'ssst-kampus', 'konferencijski-centar', 'ured-rektora', 'ured-za-finansije', 'ured-za-pravne-poslove', 'studentska-sluzba', 'it-ured', 'pr-ured', 'ssst-biblioteka', 'menadzment-univerziteta', 'partnerstvo-u-izvrsnosti', 'zasto-sarajevo', 'kontakt');
   
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   O SSST-u
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/upoznajte-ssst">Upoznajte SSST</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/poruka-rektora">Poruka Rektora</a> </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/ssst-kampus">Servisi i usluge</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-kampus">SSST kampus</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/konferencijski-centar">Konferencijski centar</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ured-rektora">Ured rektora</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ured-za-finansije">Ured za finansije</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ured-za-pravne-poslove">Ured za pravne poslove</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/studentska-sluzba">Studentska služba</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/it-ured">IT usluge</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/pr-ured">PR ured</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-biblioteka">SSST biblioteka</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/menadzment-univerziteta">Menadžment univerziteta</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/partnerstvo-u-izvrsnosti">Univerzitet Buckingham</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/zasto-sarajevo">Zašto Sarajevo</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/kontakt">Kontakt</a> </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('fakultet-kompjuterskih-nauka', 'fakultet-kompjuterskih-nauka', 'cs-nastavno-osoblje', 'poruka-cs-dekana', 'cs-program', 'cs-postdiplomski-studij', 'fakultet-informacionih-sistema', 'fakultet-informacionih-sistema', 'is-nastavno-osoblje', 'poruka-is-dekana', 'is-program', 'is-postdiplomski-studij', 'ekonomski-fakultet', 'ekonomski-fakultet', 'econ-nastavno-osoblje', 'poruka-econ-dekana', 'econ-program', 'econ-postdiplomski-studij', 'econ-publikacije', 'fakultet-politickih-nauka-i-medunarodnih-odnosa', 'fakultet-politickih-nauka-i-medunarodnih-odnosa', 'psir-nastavno-osoblje', 'poruka-psir-dekana', 'psir-program', 'psir-postdiplomski-studij', 'fakultet-stranih-jezika', 'fakultet-stranih-jezika', 'ml-nastavno-osoblje', 'poruka-ml-dekana', 'ml-program', 'cs-nastavno-osoblje', 'poruka-cs-dekana', 'engs-program', 'engs-doktorski-studij', 'studij-medicine-na-engleskom-jeziku');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Fakulteti
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/fakultet-kompjuterskih-nauka">Kompjuterske nauke</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/fakultet-kompjuterskih-nauka">O CS fakultetu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-nastavno-osoblje">CS nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-cs-dekana">Poruka CS dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-program">CS program</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-postdiplomski-studij">CS postdiplomski studiji</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/fakultet-informacionih-sistema">Informacioni sistemi</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/fakultet-informacionih-sistema">O IS fakultetu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-nastavno-osoblje">IS nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-is-dekana">Poruka IS dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-program">IS program</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-postdiplomski-studij">IS postdiplomski studiji</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/ekonomski-fakultet">Ekonomija</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/ekonomski-fakultet">O ECON fakultetu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-nastavno-osoblje">ECON nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-econ-dekana">Poruka ECON dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-program">ECON program</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-postdiplomski-studij">ECON postdiplomski studiji</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-publikacije">ECON publikacije</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/fakultet-politickih-nauka-i-medunarodnih-odnosa">Političke nauke i međunarodni odnosi</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/fakultet-politickih-nauka-i-medunarodnih-odnosa">O PSIR fakultetu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-nastavno-osoblje">PSIR nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-psir-dekana">Poruka PSIR dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-program">PSIR program</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-postdiplomski-studij">PSIR postdiplomski studij</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/fakultet-stranih-jezika">Moderni jezici</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/fakultet-stranih-jezika">O ML fakultetu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ml-nastavno-osoblje">ML nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-ml-dekana">Poruka ML dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ml-program">ML program</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Sarajevska filmska akademija</a>
         <ul class="sub-menu" >
            <li><a href="http://filmfactory.ba">film.factory</a></li>
         </ul>
      </li>
      <li>
         <a href="#"  >Inžinjerske nauke</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-nastavno-osoblje">CS nastavno osoblje</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/poruka-cs-dekana">Poruka CS dekana</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/engs-program">EngS program</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/engs-doktorski-studij">EngS Doktorski studij</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Studij medicine</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/studij-medicine-na-engleskom-jeziku">O Medicinskom fakultetu</a></li>
            <li><a href="<?=base_url('/upload/Departments/Medical-School/Medical%20School.pdf')?>">Promotivna brošura</a></li>
         </ul>
      </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('akademski-kalendar', 'studentska-sluzba', 'praksa', 'it-ured', 'biblioteka', 'studentske-aktivnosti-i-klubovi', 'enter', 'ssst-independent', 'debatni-klub', 'ssst-bend', 'ssst-sport', 'centar-za-razvoj-karijere-studenata', 'ieee-student', 'computer-graphics', 'ssst-alumni', 'convocation-2008', 'convocation-2009', 'convocation-2010', 'convocation-2011', 'convocation-2012', 'convocation-2013', 'pocasne-diplome');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Trenutni Studenti
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/akademski-kalendar">Akademski kalendar</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/studentska-sluzba">Studentska služba</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/praksa">Praksa</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/it-ured">IT ured</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/biblioteka">Biblioteka</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/studentske-aktivnosti-i-klubovi">Studentske aktivnosti</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/enter">ENTER</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-independent">SSST Independent</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/debatni-klub">Debatni klub</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-bend">SSST Bend</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-sport">SSST Sport</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/centar-za-razvoj-karijere-studenata">Students Career Center</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ieee-student">IEEE</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/computer-graphics">Kompjuterska grafika</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/ssst-alumni">Alumni</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2008">Convocation 2008</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2009">Convocation 2009</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2010">Convocation 2010</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2011">Convocation 2011</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2012">Convocation 2012</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2013">Convocation 2013</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/pocasne-diplome">Počasne diplome</a></li>
         </ul>
      </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('studijski-programi', 'prijava-za-upis-na-ssst', 'aplikacijski-formular-za-dodiplomske-studije', 'aplikacioni-formular-za-postdiplomske-studije', 'prijemni-ispiti-2014-2015', 'primjeri-prijemnih-ispita', 'finansiranje-vaseg-skolovanja-na-ssst-u','skolarina-i-studentski-krediti','stipendije-2014', 'dan-otvorenih-vrata');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Budući studenti
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/studijski-programi">Studijski programi</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/prijava-za-upis-na-ssst">Upis na SSST</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/aplikacijski-formular-za-dodiplomske-studije">Aplikacioni formular dodiplomski studiji</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/aplikacioni-formular-za-postdiplomske-studije">Aplikacioni formular postdiplomski studij</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/prijemni-ispiti-2014-2015">Prijemni ispiti</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/primjeri-prijemnih-ispita">Primjeri prijemnih ispita</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/finansiranje-vaseg-skolovanja-na-ssst-u">Školarina, studentski krediti i stipendije</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/skolarina-i-studentski-krediti">Školarina i studentski krediti</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/stipendije-2014">Stipendije 2014</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/dan-otvorenih-vrata">Dan otvorenih vrata</a></li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('jacanje-kulture-istrazivanja-i-razvoja', 'istrazivacki-instituti', 'institut-za-softverski-inzinjering-i-informacione-tehonologije', 'institut-za-nacionalne-resurse', 'institut-za-ekonomska-i-politicka-istrazivanja', 'balkanski-institut-za-izucavanje-konflikata-odgovornosti-i-pomirenja', 'istrazivacki-centrar-za-energiju-i-vodu', 'digital-media-centar', 'institut-za-jezik-i-obuku', 'projekti-i-saradnja', 'publikacije', 'pocasne-katedre-i-doktorati', 'met-fondacija');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Istraživanja
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/jacanje-kulture-istrazivanja-i-razvoja">O istraživačkom radu na SSST-u</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/istrazivacki-instituti">Istraživacki instituti</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/institut-za-softverski-inzinjering-i-informacione-tehonologije">Institut za softverski inžinjering i informacione tehonologije</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institut-za-nacionalne-resurse">Institut za nacionalne resurse</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institut-za-ekonomska-i-politicka-istrazivanja">Institut za ekonomska i politička istraživanja</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/balkanski-institut-za-izucavanje-konflikata-odgovornosti-i-pomirenja">Balkanski Institut za izučavanje konflikata, odgovornosti i pomirenja</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/istrazivacki-centrar-za-energiju-i-vodu">Istraživački Centrar za Energiju i Vodu</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/digital-media-centar">Digital media centar</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institut-za-jezik-i-obuku">Institut za jezik i obuku</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/projekti-i-saradnja">Projekti i saradnja</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/publikacije">Novije publikacije</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/pocasne-katedre-i-doktorati">Počasne katedre i doktorati</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/met-fondacija">MET fondacija</a></li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array(
       'news',
       'events',
       'media-library',
       'ssst-independent',
       'pozivi-za-aplikacije-i-konkursi'
       );
   if (in_array($cat, $ar) || in_array($uri,$ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Vijesti i događaji
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/news/ssst-prvi-akreditovani-univerzitet-u-bih">Zadnje vijesti</a></li>
      <li><a href="http://dev.ssst.edu.ba/events/inauguration-ceremony-prince-salman-chair-in-peace-and-international-studies">Događaji</a></li>
      <li><a href="#" >Obavijest za medije</a></li>
      <li><a href="http://dev.ssst.edu.ba/media-library">Media library</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/ssst-independent">SSST Independent</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/pozivi-za-aplikacije-i-konkursi">Pozivi za aplikacije i konkursi</a></li>
   </ul>
</div>
<?php endif; ?>
<?php else : ?>
<?php
   $ar = array('introducing-ssst', 'message-from-the-rector', 'ssst-campus', 'conference-center', 'rectors-office', 'finance-office', 'legal-office', 'student-services', 'it-office', 'pr-office', 'ssst-library', 'university-management', 'a-partnership-in-excellence', 'why-sarajevo', 'contact-us');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   About SSST
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/introducing-ssst">Introducing SSST</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/message-from-the-rector">Message from the Rector</a> </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/ssst-campus">Services and facilities</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-campus">SSST Campus</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/conference-center">Conference Center</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/rectors-office">Rector's Office</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/finance-office">Finance Office</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/legal-office">Legal Office</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/student-services">Student Services</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/it-office">IT services</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/pr-office">PR Office</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-library">SSST Library</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/university-management">University Management</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/a-partnership-in-excellence">University of Buckingham</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/why-sarajevo">Why Sarajevo</a> </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/contact-us">Contact Us</a> </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('computer-science-department', 'cs-faculty', 'cs-deans-message', 'cs-courses', 'cs-graduate-studies', 'department-of-information-systems', 'is-faculty', 'is-deans-message', 'is-courses', 'is-graduate-studies', 'department-of-economics', 'econ-faculty', 'econ-deans-message', 'econ-courses', 'econ-graduate-studies', 'econ-publications', 'political-science-and-international-relations-department', 'psir-faculty', 'psir-deans-message', 'psir-courses', 'psir-graduate-studies', 'modern-languages-department', 'ml-faculty', 'ml-deans-message', 'ml-courses', 'sarajevo-film-academy', 'department-of-engineering-science', 'cs-faculty', 'cs-deans-message', 'engs-courses', 'engs-graduate-studies', 'medical-studies-in-english');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Departments
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li>
         <a href="#" >Computer Science</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/computer-science-department">About CS Department</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-faculty">CS Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-deans-message">CS Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-courses">CS Courses</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-graduate-studies">CS Graduate Studies</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Information Systems</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/department-of-information-systems">About IS department</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-faculty">IS Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-deans-message">IS Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-courses">IS Courses</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/is-graduate-studies">IS Graduate Studies</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Economics</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/department-of-economics">About ECON Department</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-faculty">ECON Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-deans-message">ECON Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-courses">ECON Courses</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-graduate-studies">ECON Graduate Studies</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/econ-publications">ECON Publications</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Political Science and International Relations Department</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/political-science-and-international-relations-department">About PSIR Department</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-faculty">PSIR Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-deans-message">PSIR Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-courses">PSIR Courses</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/psir-graduate-studies">PSIR Graduate Studies</a></li>
         </ul>
      </li>
      <li>
         <a href="#" >Modern Languages</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/modern-languages-department">About ML Department</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ml-faculty">ML Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ml-deans-message">ML Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ml-courses">ML Courses</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/sarajevo-film-academy">Sarajevo Film Academy</a>
         <ul class="sub-menu" >
            <li><a href="http://filmfactory.ba">film.factory</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/department-of-engineering-science">Engineering Sciences</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-faculty">CS Faculty</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/cs-deans-message">CS Dean's Message</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/engs-courses">EngS Courses</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/engs-graduate-studies">EngS Graduate Studies</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/medical-studies-in-english">Medical School</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/medical-studies-in-english">About Medical School</a></li>
            <li><a href="<?=base_url('/upload/Departments/Medical-School/Medical%20School.pdf')?>">Prospectus</a></li>
         </ul>
      </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('academic-calendar', 'student-services', 'internship-opportunities', 'it-office', 'library', 'student-activities-and-clubs', 'enter', 'ssst-independent', 'debate-club', 'ssst-band', 'ssst-sports', 'students-career-center', 'ieee-student', 'computer-graphics', 'ssst-alumni', 'convocation-2008', 'convocation-2009', 'convocation-2010', 'convocation-2011', 'convocation-2012', 'convocation-2013', 'honorary-degrees');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Current Students
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/academic-calendar">Academic Calendar</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/student-services">Student Services</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/internship-opportunities">Internships</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/it-office">IT Office</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/library">Library</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/student-activities-and-clubs">Student Activities and Clubs</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/enter">ENTER</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-independent">SSST Independent</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/debate-club">Debate club</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-band">SSST Band</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ssst-sports">SSST Sports</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/students-career-center">Students Career Center</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/ieee-student">IEEE</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/computer-graphics">Computer Graphics</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/ssst-alumni">Alumni</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2008">Convocation 2008</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2009">Convocation 2009</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2010">Convocation 2010</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2011">Convocation 2011</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2012">Convocation 2012</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/convocation-2013">Convocation 2013</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/honorary-degrees">Honorary Degrees</a></li>
         </ul>
      </li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('degree-programmes', 'applying-to-ssst', 'application-form-for-undergraduate-studies', 'application-form-for-graduate-studies', 'entrance-exams-2014-2015', 'samples-of-entrance-exams', 'financing-your-education-at-ssst', 'fees-and-loans', 'scholarships-2014','open-day');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Prospective Students
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/degree-programmes">Degree Programmes</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/applying-to-ssst">Applying to SSST</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/application-form-for-undergraduate-studies">Application Form for Undergraduate Studies</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/application-form-for-graduate-studies">Application Form for Graduate Studies</a></li>
         </ul>
      </li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/entrance-exams-2014-2015">Entrance Exams</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/samples-of-entrance-exams">Samples of Entrance Exams</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/financing-your-education-at-ssst">Fees, Loans and Scholarships</a>
         <ul class="sub-menu">
            <li><a href="http://dev.ssst.edu.ba/static-page/fees-and-loans">Fees and Loans</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/scholarships-2014">Scholarships 2014</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/open-day">Open Day</a></li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('fostering-a-culture-of-research-and-development', 'research-institutes', 'institute-for-software-engineering-and-information-technologies', 'institute-for-national-resources', 'institute-for-economic-and-policy-research', 'balkan-institute-for-conflict-resolution-responsibility-and-reconciliation', 'the-energy-and-water-research-center', 'digital-media-center', 'institute-for-language-and-training', 'projects-and-collaborations', 'recent-publications', 'honorary-chairs-and-professorships', 'met-foundation');
   if (in_array($uri, $ar)) :
   ?>
<div class="pad ssst-blue text-center">
   Research
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/static-page/fostering-a-culture-of-research-and-development">About Research at SSST</a></li>
      <li>
         <a href="http://dev.ssst.edu.ba/static-page/research-institutes">Research Institutes</a>
         <ul class="sub-menu" >
            <li><a href="http://dev.ssst.edu.ba/static-page/institute-for-software-engineering-and-information-technologies">Institute for Software Engineering and Information Technologies</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institute-for-national-resources">Institute for National Resources</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institute-for-economic-and-policy-research">Institute for Economic and Policy Research</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/balkan-institute-for-conflict-resolution-responsibility-and-reconciliation">Balkan Institute for Conflict Resolution, Responsibility and Reconciliation</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/the-energy-and-water-research-center">The Energy and Water Research Center</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/digital-media-center">Digital media center</a></li>
            <li><a href="http://dev.ssst.edu.ba/static-page/institute-for-language-and-training">Institute for Language and Training</a></li>
         </ul>
      </li>
      <li><a href="http://dev.ssst.edu.ba/static-page/projects-and-collaborations">Projects and Collaborations</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/recent-publications">Recent Publications</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/honorary-chairs-and-professorships">Honorary Chairs and Professorships</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/met-foundation">MET Foundation</a></li>
   </ul>
</div>
<?php endif; ?>
<?php
   $ar = array('news', 'events', 'media-library', 'ssst-independent', 'calls-for-applications-and-vacancies');
   if (in_array($cat, $ar) || in_array($uri,$ar)) :
   
   ?>
<div class="pad ssst-blue text-center">
   News and Events
</div>
<div class="pad ssst-sidebar-menu">
   <ul id="sidebar-menu">
      <li><a href="http://dev.ssst.edu.ba/news">Latest News</a></li>
      <li><a href="http://dev.ssst.edu.ba/events">Events</a></li>
      <li><a href="#">Press Releases</a></li>
      <li><a href="http://dev.ssst.edu.ba/media-library">Media library</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/ssst-independent">SSST Independent</a></li>
      <li><a href="http://dev.ssst.edu.ba/static-page/calls-for-applications-and-vacancies">Calls for Applications and Vacancies</a></li>
   </ul>
</div>
<?php endif; ?>
<?php endif; ?>
