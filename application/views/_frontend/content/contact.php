<div id="main" class="clearfix">
    
<div class="grid2">
      <h2>Mi smo</h2>
      <p>Možete nas kontaktirati, pisati nam i sl i putem kontakt fomre u nastavku.</p>

            <?php if (validation_errors() != "") : ?> 
            <hr>
              <div class="form-status">
                      <div class="error"><?=validation_errors()?></div>
                </div>
            <hr>
             <?php elseif (isset($thankyou) != '') : // vea fix - GROZNA provjera ?>
            <hr>
              <div class="form-status">
                <span style="color: #990000;"><?php echo set_value('name'); ?></span>, Vaša poruka je uspješno poslana.<br />
                </div>
            <hr>
            <?php endif; ?>
            <?php if (!isset($thankyou)) : // vea fix - GROZNA provjera ?>
                    <?php echo form_open("/contact/send"); ?>
                        <ul>
                          <li>
                            <?php echo form_label("Ime <span>(obavezno)</span>","name"); ?>
                            <?php echo form_input('name',set_value('name'),'required="" placeholder="Vaše ime i prezime" class="required"'); ?>
                          </li>
                          <li>
                            <?php echo form_label("Organizacija","organization"); ?>
                            <?php echo form_input('organization',set_value('organization'),'placeholder="Naziv organizacije"'); ?>
                          </li>
                          <li>
                            <?php echo form_label("Email <span>(obavezno)</span>","email"); ?>
                            <?php // vea fix - iz nekog razloga u codeigniteru nema mogucnost da podesi type za input polje ... echo form_input('email',set_value('email'),'type="email" placeholder="Vaša e-mail adresa" class="required"'); ?>
                              <input type="email" value="<?=set_value('email')?>" placeholder="Vaša e-mail adresa" id="email" name="email" class="valid" class="required">
                          </li>
                          <li>
                            <?php echo form_label("Predmet <span>(obavezno)</span>","subject"); ?>
                            <?php echo form_input('subject',set_value('subject'),'placeholder="Predmet poruke" class="required"'); ?>
                          </li>
                          <li>
                            <?php echo form_label("Poruka <span>(obavezno)</span>","message"); ?>
                            <?php echo form_textarea('message',set_value('message'),'placeholder="Predmet poruke" class="required"'); ?>
                          </li>
                          <li>
                            <?php echo form_label("Provjera sa slike ispod <span>(obavezno)</span>","captcha"); ?>
                            <?php echo form_input('captcha',set_value('captcha'),'placeholder="Provjera sa slike ispod..." class="required"'); ?><br /><br />
                            <?php echo $captcha['image']; ?>
                          </li>
                          <li>
                            <button>Pošalji</button>
                          </li>
                        </ul>
                      <?php echo form_close(); ?>
            <?php endif; ?>          
</div>
    
    <div class="grid3 reset vcard">
      	<h2>Our Office</h2>
		<div class="adr">
      		<div class="street-address">Grbavička 6A</div>  
    		<span class="postal-code">71 000</span>,
      		<span class="locality">Sarajevo</span>,
<!--    		<span class="region"></span>-->
    		<span class="country-name">Bosna i Hercegovina</span>
    	</div>
		<p>
			<strong>Telefon:</strong> <span class="tel">+387 / 61 255 071</span><br>
			<strong>Email:</strong> <a href="mailto:info@spaja-lica.com" class="email">info@spaja-lica.com</a>
      </p>
      <h2>Our Location</h2>
      <img src="/images/illustrations/map.jpg">
  </div>
</div>