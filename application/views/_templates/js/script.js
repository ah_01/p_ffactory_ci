/* Author: Sitebase */

/* Top scroll handling */
// Hide top button
$('#top-link').hide();
$(window).scroll(function() {
	var scroll_pos = $(window).scrollTop();
	if(scroll_pos > 100 && scroll_pos < 1000){
		$('#top-link').show();
		var opacity_val = Math.floor(((scroll_pos/1000) * 0.6) * 10) / 10;
		$('#top-link').css('opacity', opacity_val);
	}else if(scroll_pos < 100){
		$('#top-link').hide();	
	}
	
});
$('#top-link').click(function(){ 
	$(this).hide();
	$('html, body').animate({scrollTop:0}, 'slow');
});

/* Add innerfade to gallery */
$('#gallery ul').innerfade({
	speed: 1000,
	timeout: 5000,
	type: 'sequence',
	containerheight: '200px'
});





















