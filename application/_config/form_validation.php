<?php
// vea add - STUPID HACK HORRIBLE AVFLEE
$trajanje_studija = (isset($_POST['trajanje_studija'])) ? $_POST['trajanje_studija'] : '';
$session_id = (isset($_POST['session_id'])) ? $_POST['session_id'] : '';
$godina_studija = (isset($_POST['godina_studija'])) ? $_POST['godina_studija'] : '';

$config = array(

'content_save' => array(
                array('field' => 'status','label' => 'Status','rules' => 'trim|xss_clean'),
                array('field' => 'category_id','label' => 'Kategorija','rules' => 'trim|xss_clean'),
                array('field' => 'subject','label' => 'Subject','rules' => 'trim|min_length[3]|max_length[500]|xss_clean'),
                array('field' => 'source','label' => 'Source','rules' => 'trim|min_length[3]|max_length[500]|xss_clean'),
                array('field' => 'author','label' => 'Author','rules' => 'trim|min_length[3]|max_length[500]|xss_clean'),
                array('field' => 'title','label' => 'Title','rules' => 'trim|required|min_length[5]|max_length[500]|xss_clean'),
                array('field' => 'url','label' => 'Url','rules' => 'trim|min_length[3]|max_length[100]|xss_clean'),
                array('field' => 'image','label' => 'Image','rules' => 'trim|xss_clean'),
                array('field' => 'date_create','label' => 'Date of create','rules' => 'required'),
                array('field' => 'date_update','label' => 'Date of update','rules' => ''),
                array('field' => 'date_order','label' => 'Date for order','rules' => ''),
                array('field' => 'introtext','label' => 'Uvodni tekst','rules' => 'trim|min_length[10]|max_length[700]'),
                array('field' => 'fulltext','label' => 'Glavni text','rules' => 'trim|required|min_length[10]|max_length[10000]'),
                array('field' => 'tags','label' => 'Tags','rules' => 'trim|required|min_length[3]|max_length[1000]'),
                array('field' => 'tags','attachments' => 'Attachments','rules' => 'trim|required|min_length[3]|max_length[50000]'),
                array('field' => 'tags','gallery' => 'Gallery','rules' => 'trim|required|min_length[3]|max_length[50000]'),
                ),

'contact_send' => array(
                array('field' => 'name','label' => 'ime i prezime','rules' => 'trim|required|min_length[3]|max_length[100]'),
                array('field' => 'email','label' => 'e-mail adresa','rules' => 'trim|required|valid_email|min_length[5]|max_length[120]|xss_clean'),
                array('field' => 'subject','label' => 'Predmet','rules' => 'trim|required|min_length[5]|max_length[100]|xss_clean'),
                array('field' => 'message','label' => 'Poruka','rules' => 'rim|required|min_length[3]|max_length[1000]|xss_clean'),
                array('field' => 'captcha','label' => 'Sigurnosna potvrda','rules' => 'trim|required|numeric|exact_length[4]|callback_check_captcha')
                ),

'category_save' => array(
                array('field' => 'name','label' => 'Name','rules' => 'trim|required|min_length[3]|max_length[100]|xss_clean'),
                array('field' => 'url','label' => 'URL','rules' => 'trim|min_length[3]|max_length[100]|xss_clean'),
                array('field' => 'description','label' => 'Description','rules' => 'trim|min_length[3]|max_length[100]|xss_clean'),
                array('field' => 'icon','label' => 'Icon','rules' => 'trim'),
                array('field' => 'image','label' => 'Image','rules' => 'trim'),
                array('field' => 'articles_per_page','label' => 'Articles Per Page','rules' => 'trim|required|numeric'),
                array('field' => 'layout','label' => 'Layout','rules' => 'trim|required'),
                array('field' => 'tags','label' => 'Tags','rules' => 'trim|required'),
                array('field' => 'sidebar_content','label' => 'Sidebar content','rules' => ''),
                array('field' => 'number_of_linx','label' => 'Number of linx','rules' => 'trim|required|numeric'),
                ),

'settings_save' => array(
                array('field' => 'path_to_layouts','label' => 'Name','rules' => 'trim|required|xss_clean'),
                array('field' => 'path_to_article_layouts','label' => 'URL','rules' => 'trim|required|xss_clean'),
                array('field' => 'main_view','label' => 'Description','rules' => 'trim|required|xss_clean'),
                array('field' => 'contact_email','label' => 'Icon','rules' => 'trim|required|valid_email'),
                array('field' => 'frontpage_slider','label' => 'Image','rules' => 'trim|required'),
                array('field' => 'analytics_code','label' => 'Articles Per Page','rules' => 'trim|required'),
                ),

'konkurs_form_validate' => array(
                array('field' => 'konkurs_id','label' => 'ID Konkursa','rules' => 'trim|required|numeric|xss_clean|exact_length[1]'),
                array('field' => 'ime','label' => 'Ime','rules' => 'trim|required|xss_clean|min_length[3]|max_length[100]'),
                array('field' => 'ime_roditelja','label' => 'Ime jednog roditelja','rules' => 'trim|required|xss_clean|min_length[3]|max_length[100]'),
                array('field' => 'prezime','label' => 'Prezime','rules' => 'trim|required|xss_clean|min_length[1]|max_length[100]'),
                array('field' => 'telefon','label' => 'Telefon','rules' => 'trim|required|numeric|xss_clean|exact_length[11]'),
                array('field' => 'mobitel','label' => 'Mobitel','rules' => 'trim|required|numeric|xss_clean|min_length[11]|max_length[12]|callback_check_mobile|is_unique_filter[konkursi.mobitel.status.-1]'),
                array('field' => 'email','label' => 'Email','rules' => 'trim|required|valid_email|xss_clean|max_length[200]|is_unique_filter[konkursi.email.status.-1]'),
                array('field' => 'mobitel','label' => 'Mobitel','rules' => 'trim|required|numeric|xss_clean|exact_length[11]|callback_check_mobile|callback_is_unique_active[Mobitel]'),
                array('field' => 'email','label' => 'Email','rules' => 'trim|required|valid_email|xss_clean|max_length[200]|callback_is_unique_active[Email]'),
                array('field' => 'adresa','label' => 'Adresa','rules' => 'trim|required|xss_clean|min_length[5]|max_length[100]'),
                array('field' => 'opstina','label' => 'Općina / grad / distrikt','rules' => 'trim|required|numeric|xss_clean|min_length[1]|max_length[3]|greater_than[0]|less_than[142]'),
                array('field' => 'fakultet','label' => 'Fakultet na kojem studirate','rules' => 'trim|required|numeric|xss_clean|min_length[1]|max_length[2]|greater_than[0]|less_than[27]'),
                array('field' => 'trajanje_studija','label' => 'Trajanje studija','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|greater_than[2]|less_than[7]'),
                array('field' => 'godina_studija','label' => 'Godina studija','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|greater_than[1]|less_than[7]|callback_check_godina_studija['.$trajanje_studija.']'),
                array('field' => 'prosjek_ocjena','label' => 'Prosjek ocjena','rules' => 'trim|required|decimal|xss_clean|greater_than[5]|less_than[10]|callback_check_prosjek['.$godina_studija.']'),
//                array('field' => 'jmbg','label' => 'Vaš Jedinstveni Matični Broj Građana (JMBG)','rules' => 'trim|required||xss_clean|exact_length[13]|is_unique_filter[konkursi.jmbg.status.-1]'),
//                array('field' => 'broj_licne_karte','label' => 'Broj lične karte','rules' => 'trim|required|alpha_numeric|xss_clean|exact_length[9]|is_unique_filter[konkursi.broj_licne_karte.status.-1]'),
                array('field' => 'datum_rodjenja','label' => 'Datum rođenja','rules' => 'trim|required|xss_clean'),
                array('field' => 'mjesto_rodjenja','label' => 'Mjesto rođenja','rules' => 'trim|required|xss_clean|max_length[100]'),
                array('field' => 'gender','label' => 'Spol/Sex/Gender','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'ukupna_primanja','label' => 'Ukupna primanja svih članova domaćinstva','rules' => 'trim|required|numeric|xss_clean|min_length[1]|less_than[10000]'),
                array('field' => 'ukupno_clanova','label' => 'Ukupan broj članova domaćinstva','rules' => 'trim|required|numeric|xss_clean'),
                array('field' => 'roditelji','label' => 'Status roditelja','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|greater_than[0]|less_than[4]'),
                array('field' => 'komentar','label' => 'Komentar ili pitanje','rules' => 'trim|xss_clean|min_length[0]|max_length[1000]'),
                array('field' => 'porodica_rvi','label' => 'Da li je neki drugi od užih članova vaše porodice RVI?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'univerzitet_sarajevo','label' => 'Da li ste student Univerziteta u Sarajevu?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'redovan','label' => 'Da li se prvi put upisujete u akademsku 2012-2013 godinu?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'uvjerenje_redovno_skolovanje','label' => 'Da li možete dostaviti uvjerenje o redovnom školovanju?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'uvjerenje_polozeni_ispiti','label' => 'Da li možete dostaviti  uvjerenje o položenim ispitima?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'prima_stipendiju','label' => 'Dali primate stipendiju ili novčanu naknadu od nekog drugog pravnog ili fizičkog lica?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'izjava_o_neprimanju_stipendije','label' => 'Da li možete dostaviti izjavu da ne primate stipendiju ili novčanu naknadu od nekog drugog pravnog ili fizičkog lica?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'stalno_prebivaliste','label' => 'Da li imate stalno prebivalište u Bosni i Hercegovini?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'fotografije','label' => 'Da li možete dostaviti dvije originalne fotografije 3,5 x 4,5 cm?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'samo_jedna_prijava','label' => 'Da li je za ovaj konkurs aplicirao vaš brat ili sestra?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'uvjerenje_domacinstvo','label' => 'Da li možete dostaviti uvjerenje o zajedničkom domaćinstvu?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'dokaz_o_primanjima','label' => 'Da li možete dostaviti dokaz o mjesečnim primanjima  za svakog člana pojedinačno ili dokaz da nema primanja?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'dokaz_o_rvi','label' => 'Ako je neko od užih članova porodice RVI, civilna žrtva rata ili poginuo, da li možete dostaviti dokaz za to?','rules' => 'trim|required|numeric|xss_clean|exact_length[1]|less_than[2]'),
                array('field' => 'captcha','label' => 'Sigurnosna potvrda','rules' => 'trim|required|numeric|exact_length[4]|callback_check_captcha')
                ),

'sms_validate' => array(
                array('field' => 'session_id','label' => 'Sesija','rules' => 'trim|required|alpha_numeric'),
                array('field' => 'sms_kod','label' => 'SMS kod','rules' => 'trim|required|alpha_numeric|exact_length[6]|callback_check_sms_kod['.$session_id.']'),
                ),
);
