<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CI_Controller {

	public function index()
	{
		$this->load->library('migration');

		if (!$this->migration->current()) {
			show_error($this->migration->error_string());
			$this->session->set_flashdata('error', $this->migration->error_string());
			

		}else{
			echo "Migration sucess!";
			$this->session->set_flashdata('error', 'Migration success');
			redirect('/emails/tasks');
		}
	}

}

/* End of file migration.php */
/* Location: ./application/controllers/migration.php */