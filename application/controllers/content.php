<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends CI_Controller {

    public $view = Array(
            'javascript' => Array(),
            'css' => Array(),
            'header' => Array(),
            'footer' => Array(),
            'theend' => 'frontend',
            'content' => '',
            'layout' => '2columns-right',
            'subheader' => array('media_library'),
            'mainbar' => array('content'),
            'sidebar' => array(),
        );

        function __construct() {
            parent::__construct();
            date_default_timezone_set('Europe/Sarajevo');
            $this->load->model('Content_model', 'content');
            $this->language();
        }

        public function index() {
            // vea add - some kind of archive
        }

        public function category() {

            $category = $this->uri->segment(1); // vea add - must be called like this!
            $language = $this->session->userdata('language');
            $data['category'] =$this->content->get_category($category);

            $this->load->library('pagination');

            $config['base_url'] = base_url($data['category']['url'].'/page');
            $config['total_rows'] = $this->db->where('language',$language)->where('category_id',$data['category']['id'])->where('status',1)->get('content')->num_rows();
            $config['per_page'] = $data['category']['articles_per_page'];
            $config['num_links'] = 6;

            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';

            $config['next_link'] = '&raquo;';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&laquo;';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';

            $config['last_link'] = '<strong>&raquo;&raquo;</strong>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['first_link'] = '<strong>&laquo;&laquo;</strong>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';

            $this->pagination->initialize($config);

            $data['title'] = $data['category']['name'];
            $data['page_description'] = $data['category']['description'];

            $offset = $this->uri->segment(3);
            $data['articles'] = $this->content->get_content($data['category']['id'],$data['category']['articles_per_page'],$offset);
            $data['sidebar_links'] = $this->content->get_sidebar_links($data['category']['id'],$data['category']['number_of_linx']);
            $data['sidebar_content'] = $this->content->get_sidebar_content($data['category']['id']);

            // vea add - shit, shoud be realted to some settings from DB!!!
            $data['categories'] = $this->content->get_categories();
            $data['news'] = $this->db->where('category_id','18')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            //$data['events'] = $this->db->where('category_id','14')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            $data['media_library'] = $this->db->where('category_id','16')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();


            $data['view'] = $this->view;
            $data['view']['layout'] = '2columns-left';
            $data['view']['mainbar'] = 'blog';

            $this->load->view('main',$data);
        }

        // objediniti ove dvije metode
        public function article() {
            $language = $this->session->userdata('language');

            $cat = $this->uri->segment(1);
            $url = $this->uri->segment(2);

            $data['category'] =$this->content->get_category($cat);
            
            $data['category']['article_layout'] = 'long-article'; // vea add - dirty fix
            //$data['category']['article_layout'] = 'simple-article'; // vea add - dirty fix

            // $data['page_description'] = $data['category']['description']; // + item title
            $data['page_description'] = ""; // + item title

            $data['articles'] = $this->content->get_article($url,$data['category']['id']);

            $data['tag_cat'] = $data['articles'][0]['tags'];


            if ($data['tag_cat'] == 'faculty') { //select faculty based articles only to show weher article has faculty tag
            
                $data['rel_cat'] = $this->db->where('category_id','21')->order_by('title','asc')->get('content',5)->result_array();
            // extracted tag value for list of related articles - same cat name //var_dump($data['articles'][0]['tags']);
            // echo of tag //echo $data['tag_cat'];
            }

            //var_dump($data['articles']);
            //$data['sidebar_links'] = $this->content->get_sidebar_links($data['category']['id'],$data['category']['number_of_linx']);
            //$data['sidebar_content'] = $this->content->get_sidebar_content($data['category']['id']);

            //$data['news'] = $this->db->where('category_id',$news_category)->where('status','1')->order_by('date_update','desc')->get('content')->result_array(); // vea add - cat news = id 12

            // vea add - shit, shoud be realted to some settings from DB!!!
            $data['categories'] = $this->content->get_categories();
            //$data['news'] = $this->db->where('category_id','18')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            //$data['events'] = $this->db->where('category_id','14')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            $data['media_library'] = $this->db->where('category_id','16')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();

            $data['view'] = $this->view;
            $data['title'] = $data['category']['name']; // + item title
            $data['page_title'] = "SSST";//$data['articles']['0']['title']; // + item title
            $data['view']['layout'] = '2columns-left';
            $data['view']['mainbar'] = 'blog';

            $this->load->view('main',$data);
        }

        public function send_to_print() {

        }

        public function send_to_pdf() {

        }

        public function rss() {

        }

        public function language() {
                $language = $this->session->userdata('language');
                if(!isset($language) || $language == '') {
                    $this->session->set_userdata('language','english');
                } else {
                    $this->session->set_userdata('language',$language);
                    $this->config->set_item('language', $language);
                    $this->lang->is_loaded = array();
                    $this->lang->load('interface', $language);
                }
            }

        public function change_language() {
                $language = $this->uri->segment(2);
                $this->session->set_userdata('language',$language);
                redirect(base_url());
            }

}