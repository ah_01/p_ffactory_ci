<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dg_test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('datagrid','url'));
		$this->Datagrid = new Datagrid('emails', 'id');

		var_dump($_POST);

	}

	public function index()
	{




		$this->load->helper('form');
		$this->load->library('session');

		// var_dump($this->Datagrid);


		$data['subview'] = 'datagrid';
		$this->load->view('email/email_layout', $data);

	}

	function proc($request_type = '')
	{

		// $this->load->helper('url');
		if ($action = Datagrid::getPostAction()) {
			$error = '';
			switch ($action) {
				case 'delete':
					if (!$this->Datagrid->deletePostSelection()) {
						$error = 'Items clud not be deleted';
					}
					break;

				// default:
				// 	# code...
				// 	break;
			}

			if ($request_type != 'ajax') {
				// Load lib session
				$this->session->set_flashdata('form_error', $error);
				redirect('dg_test/index');

			}else{
				echo json_encode(array('error' => $error));
			}

		}else{
				die('Bad request');
			}
	}

}

/* End of file dg_test.php */
/* Location: ./application/controllers/dg_test.php */