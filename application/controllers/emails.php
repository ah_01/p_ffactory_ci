<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('emails_model', 'm');
		// Login check - move to helper function or?
        // $this->load->model('Admini_model', 'a');
        // $this->a->_is_logged_in();
        $id = $this->uri->segment(3);
	}

	public function index()
	{

		if (isset($data['arr_emails'])) {
			var_dump($arr_emails);
		}


		// var_dump($data['letters']);


		$data['subview'] = 'index';
		$this->load->view('email/email_layout', $data);

	}


	/**
	 * Function recipients gets all emails from DB and passess them as  array to list view 
	 * @return Array
	 */
	public function recipients()
	{
		$this->load->library('pagination');
		$this->load->helper('date');



		// Date format
		$format = 'DATE_RFC822';



					$query = $this->db->get('emails');
					if ($query->num_rows()>0) {
						foreach ($query->result() as $row) {

							$row->created = standard_date($format, mysql_to_unix($row->created));
							//print_r($row->created);

							$arr[] = $row;
						}
					}


					$data['results'] = $arr;




		$data['subview'] = 'list';
		$this->load->view('email/email_layout', $data);

	}

	/**
	 * Recipient delete - delete recipient from emails table based on ID
	 * @return [none]
	 */
	public function rdelete()
	{
		$id = $this->uri->segment(3);

		$this->m->delete($id, 'emails');
		redirect('/emails/recipients');
	}

	/**
	 * Tasks - maintenance function for various tasks (migration)
	 */
	public function Tasks()
	{
		// var_dump(is_dir('/home/vagrant/apps/spajalica.com/upload/attachments/'));
		// var_dump($_SERVER['SCRIPT_FILENAME']);

		$this->config->load('migration');

		$data['subview'] = 'tasks';
		$this->load->view('email/email_layout', $data);

	}

	/**
	 * read file or string from form, \parse value for emails and insert emails in Table emails
	 * @return [type]
	 */
	function read()
	{
		$this->load->helper('email');
		$this->load->library('form_validation');
		$string = $this->input->post('emails');

		if ($_POST['file']) {
			
			$file = site_url($_POST['file']);
			
			// var_dump(FCPATH . $_POST['file']);


			if (file_exists(FCPATH . $_POST['file'])) {
				
				$emails = parse_emails($string, $file);

				
			}else{
				die('File does not exsist;');
			}
			

			
		}else{

			$emails = parse_emails($string);
		}
		// print_r($string);

		// //Array prepare
		// $string = preg_replace("/\s[\s]+/",",",$string);
		// $string = preg_replace("/\n+/",",",$string);
		// $emails = explode(",",$string);

		// var_dump($emails);

		// var_dump($array);
		$array = array();
		$data['email_messages'] = null;
		foreach ($emails as $email) {


			if (valid_email($email)) {
				if($this->email_exist($email) == FALSE) {
					$this->db->insert('emails', array('email'=>$email));
					$data['email_messages'] .= "$email inserted in DB <br>";
				}else{
					$data['email_messages'] .=  "$email already exsists! <br>";
				}
			}else{
				$data['email_messages'] .= "$email is not valid email! <br>";
			}



		}
		// var_dump($array);

		// if($this->db->insert_batch('emails', $array)){
		// 	echo "Insert done";
		// }

		if (!$_POST['file']) {
			$data['subview'] = 'index';
			$this->load->view('email/email_layout', $data);
		}else{
			echo alert_m($data['email_messages']);
		}

	}

	/**
	 * Recipient edit - function called by ajax from footer of file to change / edit group for recipient
	 * @return [message]
	 */
	function redit()
	{
		$id = $_POST['id'];
		$mgroup = $_POST['mgroup'];

				$insert = $this->m->insert('emails', array('mgroup'=> $mgroup), $id);
				if ($insert) {
				 	alert_m($insert, 'alert-success');
				 } 

	}

	/**
	 * Retrieve email letter - based on presence of ID, if ID is present article is retrieved else list of article is retrieved
	 * @return [view]
	 */
	function edit()
	{


		//check if id of article is set and assign $id value
		$this->uri->segment(3) ? $id=$this->uri->segment(3) : $id = NULL;



		// if id is not set create new article

		if ($id != NULL) {
			// if ID is set get message values for edit of exsiting article
			$data['letters'][] =$this->m->get('letters', 'desc', $this->uri->segment(3));
			$data['subject'] = $data['letters'][0]->subject;
			$data['body'] = $data['letters'][0]->body;

		}else{

			//Ger email  messages from DB
				$data['letters'] =$this->m->get('letters', 'desc');
				$data['subject'] = '';
				$data['body'] = '';
			}


		// var_dump($data['letters']);
		$data['subview'] = 'emails';
		$this->load->view('email/email_layout', $data);

	}

	/**
	 * [delete email letter based on id]
	 * @return [type]
	 */
	function delete()
	{
		$id = $this->uri->segment(3);

		$this->m->delete($id, 'letters');
		redirect('/emails/edit');

	}

	/**
	 * [add_message description - data is retrieved from post data provided by form, if ID is present entry is updated if no ID new email letter is added]
	 */
	function add_message()
	{


		$data = $this->m->array_from_post(array('subject', 'body'));
		foreach ($data as $member) {
		// var_dump($data);
			if ($member == '') {

				$this->session->set_flashdata('error', 'Please fill all fields!');
				redirect('/emails/edit');

			}
		}

		$this->session->set_flashdata('error', ($data['subject'] . '-' . $data['body']));
		
		if ($this->uri->segment(3)) {
			// Update email
			$retId = $this->m->insert('letters', $data, $this->uri->segment(3));
		}else{

			// Add new letter
			$retId = $this->m->insert('letters', $data);

		}
		redirect('/emails/edit');
		// var_dump($retId);
	}


	/**
	 * [email_exist description - check if email exists in DB]
	 * @param  [string] $email
	 * @return [bool]
	 */
	function email_exist($email)
	{
		$this->db->where('email', $email);
		$q = $this->db->get('emails');

		foreach ($q->result() as $r) {
			if ($r->email) {
				return TRUE;
			}else{
				return FALSE;

			}
		}
	}

	///////////////////////////////////////////////////////////////////

	/**
	 * [upload_file description - obsolete function for file upload]
	 * @return [type]
	 */
	public function upload_file()
	{
		$status = "";
		$msg = "";
		$file_element_name = 'userfile';

		if (empty($_POST['title'])) {
			$status = "error";
			$msg = "Please enter a title";
		}


		if ($status != "error") {

			$config['upload_path'] = '/home/vagrant/apps/spajalica.com/upload/attachments';
			$config['allowed_types'] = 'gif|jpg|png|pdf';
			$config['max_size']  = '1024 * 7';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload($file_element_name)){
				$status = "error";
				$msg = $this->upload->display_errors('', '');
			}
			else{
				$data = $this->upload->data();
				$file_id = $this->files_model->insert_file($data['file_name'], $_POST['title']);

				if ($file_id) {
					$status = 'success';
					$msg = "File sucessfully uploaded";
				}else{
					unlink($data['full_path']);
					$status = "error";
					$msg = "Something wen wrong when saving file";

				}
			}

			@unlink($_FILES['file_element_name']);
		}


		$data['upload'] = array('status'=> $status, 'msg'=>$msg);
		redirect('emails');

	}


}

/* End of file emails.php */
/* Location: ./application/controllers/emails.php */
