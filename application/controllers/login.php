<?php

class Login extends CI_Controller {

    function index() {

     /*   echo "<pre>";
        var_dump($this->session->all_userdata());
        echo "</pre>";*/

        $this->load->view('backend/login');
    }

    function validate() {
        
        $this->output->enable_profiler(TRUE);


        $this->load->model('login_model');
        $data = $this->login_model->validate();


        if ($data != false) {
            $data['is_logged_in'] = true;
            $this->session->set_userdata($data);
            redirect('admini');
        }
        else {
            $this->index();
        }
    }

    function logout() {
        $this->session->sess_destroy();
        $this->index();
    }

}
