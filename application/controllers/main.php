<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    public $view = Array(
                'theend' => 'frontend',
                'content' => 'frontpage',
                'layout' => '2columns-right',
                'subheader' => array('slider'),
                'mainbar' => array('content'),
                'sidebar' => array('news'),
            );

        function __construct() {
                parent::__construct();
                date_default_timezone_set('Europe/Sarajevo');
                $this->load->model('Content_model', 'content');
                $this->language();
            }

    public function index() {
            // vea add - language hack
            // $language = $this->session->userdata('language');

            // $data['categories'] = $this->content->get_categories();

            // $data['news'] = $this->db->where('category_id','20')->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            // $data['events'] = $this->db->where('category_id','14')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            // $data['media_library'] = $this->db->where('category_id','16')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();

            // $data['view'] = $this->view;
            // $data['view']['layout'] = '1column';
            // $data['view']['content'] = 'frontpage';
            // $data['page_description'] = '';

            // $this->load->model('Content_model', 'content');


            // $main_view = $this->db->where('key','main_view')->get('settings')->row()->value; // vea add - treba da je switch za on/off
            // $this->load->view($main_view,$data);

            redirect('static-page/welcome');

    }

    public function search() {

            $language = $this->session->userdata('language');
            $offset = $this->uri->segment(3);
            $data['term'] = $this->_search_term($this->input->post('search'));

            $data['categories'] = $this->content->get_categories();
            $data['title'] = "Search @ SSST";
            $data['page_description'] = "Search @ SSST for: ".$data['term'];

            // $data['news'] = $this->db->where('category_id','18')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            // $data['events'] = $this->db->where('category_id','14')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();
            // $data['media_library'] = $this->db->where('category_id','16')->where('language',$language)->where('status','1')->order_by('date_create','desc')->get('content',5)->result_array();

            //$data['sidebar_content'] = $this->content->get_sidebar_content($data['category']['id']);

            if ($data['term'] != false) {
                $data['articles'] = $this->db->having('status',1)->having('language',$language)->like('title', $data['term'], 'both')->or_like('fulltext', $data['term'], 'both')->get('content',10,$offset)->result_array();
            } else {
                redirect($this->agent->referrer());
            }

            $this->load->library('pagination');

            $config['base_url'] = base_url('search/page');
            $data['total_rows'] = $config['total_rows'] = $this->db->having('status',1)->having('language',$language)->like('title', $data['term'], 'both')->or_like('fulltext', $data['term'], 'both')->get('content')->num_rows();
            $config['per_page'] = 10;
            $config['num_links'] = 6;

            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';

            $config['next_link'] = '&raquo;';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['prev_link'] = '&laquo;';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';

            $config['last_link'] = '<strong>&raquo;&raquo;</strong>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['first_link'] = '<strong>&laquo;&laquo;</strong>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';

            $this->pagination->initialize($config);


            $data['category']['article_layout'] = 'short_article';
            $data['view'] = $this->view;
            $data['view']['layout'] = '2columns-left';
            $data['view']['mainbar'] = 'search';
            $data['view']['sidebar'] = array('search','boxes');
            $data['view']['subheader'] = 'media_library';

            $this->load->model('Content_model', 'content');

            //$this->output->enable_profiler(TRUE);

            $main_view = $this->db->where('key','main_view')->get('settings')->row()->value; // vea add - treba da je switch za on/off
            $this->load->view($main_view,$data);
    }

    public function _search_term ($term = '') {

            $search = $this->session->userdata('search');
            $asdf = filter_var(trim($term), FILTER_SANITIZE_STRING);

            if ($asdf == '' && (isset($search) && $search != '')) {
                return $search;
            } elseif ($asdf != '') {
                $this->session->set_userdata('search',$asdf);
                return $asdf;
            } else {
                return false;
            }
        }

    public function language() {
            $language = $this->session->userdata('language');
            if(!isset($language) || $language == '') {
                $this->session->set_userdata('language','english');
            } else {
                $this->session->set_userdata('language',$language);
                $this->config->set_item('language', $language);
                $this->lang->is_loaded = array();
                $this->lang->load('interface', $language);
            }
        }

    public function change_language() {
            $language = $this->uri->segment(2);
            $this->session->set_userdata('language',$language);
            redirect(base_url());
        }

    function check_captcha() {
        $this->load->helper('captcha_functions'); return check_captcha(); //ne znam iz kojeg razloga nece da povuce sam funkciju iz helpera;
    }

    public function emailsendchron() {
        $emails = $this->db->get('marketingemails')->result_array();
        echo $num = $this->uri->segment(2);
        // var_dump($emails);
    }

    public function pool() {
                    // vea add - neka vrsta ankete...
    }

    public function menu() {
        // vea add - neka vrsta statistike se skuplja preko ovoga
        $data['view']['layout'] = '1column';
        $data['view']['mainbar'] = 'menu';
        $data['view']['sidebar'] = array();
        $data['view']['subheader'] = '';
        $data['articles'] = array();

        $data['view'] = $this->view;
        $this->load->view('frontend/wrapper',$data);
    }

}
