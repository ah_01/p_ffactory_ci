<?php

/**
*
*/
class Emails_Model extends CI_Model
{

	function array_from_post($fields)
	{
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->input->post($field);
		}
		//var_dump($data);
		return $data;
	}


	function insert($table, $fields, $id=NULL)
	{
		$this->db->set($fields);
		if ($id == NULL) {
			# code...
			$this->db->insert($table);
		}else{
			$this->db->where('id', $id);
			$this->db->update($table);
		}

		return $this->db->affected_rows();

	}

	public function insert_file($filename, $title)
	{
		$data = array(
			'filename'	=> $filename,
			'title'		=> $title
			);

		$this->db->insert('files', $data);
		return $this->db->insert_id();
	}

	public function get($table, $desc = 'asc', $id=NULL, $single=NULL)
	{

		if ($id != NULL) {
			// Filter $id
			$id = intval($id);

			$this->db->where('id', $id);
			$method = 'row';
		}else{
			$method = 'result';
		}



		return $this->db->order_by('id', $desc)
				->get($table)
				->$method();
	}

	public function delete($id, $table)
	{
		$id = intval($id);

		if (!$id) {
			return FALSE;
		}

		$this->db->where('id', $id);
		$this->db->limit(1);

		$this->db->delete($table);

	}
}