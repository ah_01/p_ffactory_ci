<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admini_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('form_validation');
    }

    function dashboard($id = 1) {
        $this->db->where( 'id', $id );
        $query = $this->db->get('stranice', 1);
        return $query->row_array();
    }

/**************************************************************************************************************
*********************************************** CATEGORIES **************************************************/

    function get_categories() {

        // vea add - ovdje moram da napravim jednu igrariju s obzirom da ovo cudo vraca id unutar podniza a meni bi bilo najzgodnije da je id ustvari kljuc niza
        $tmp = $this->db->get('categories')->result_array();
        $data = array();
            foreach ($tmp as $category) { // ovo treba da bude fja dodata u CI fw u dvije-tri verzije subnode(), subnodes($array,$array,[koji key hocemo]),
                $data[$category['id']] = $category;
            }

        return $data;
    }

    function get_category($id) { // iako je dosta jednostavniji scenarij nego "content" metoda, odlucio sam da ovo rastavim cisto radi semantike

        $this->db->where('id',$id);
        $data = $this->db->get('categories')->row_array();

        return $data;
    }

    function save_category() {

            $category = Array (
                    "name" => $this->input->post("name"),
                    "url" => ($this->input->post("url") != "") ? $this->input->post("url") : strtolower(url_title(convert_accented_characters($this->input->post("name")))),
                    "description" => $this->input->post("description"),
                    "icon" =>  $this->input->post("icon"),
                    "image" =>  $this->input->post("image"),
                    "articles_per_page" => $this->input->post("articles_per_page"),
                    "layout" => $this->input->post("layout"),
                    "article_layout" => $this->input->post("article_layout"),
                    "tags" => $this->input->post("tags"),
                    "sidebar_content" =>  ($this->input->post("sidebar_content") != NULL) ? implode(',', $this->input->post("sidebar_content")) :'',
                    "number_of_linx" => $this->input->post("number_of_linx"),
                    );

            if ($this->input->post('id') == '') {
                $this->db->insert('categories', $category);
            } elseif (is_numeric($this->input->post('id'))) {
                $this->db->where('id', $this->input->post('id'));
                $this->db->update('categories', $category);
            }

                return true;

    }

    function delete_category($id = '') {

        if ($this->session->userdata('role') == 0 && $id != '') {
            $this->db->where('id',$id);
            $query = $this->db->delete('categories');
            return true;
        } else {
            return false;
        }

    }

/***************************************************************************************************************
************************************************* CONTENT ****************************************************/

    function save_content() { #updates or insert if item dooesn't exist...  switch is in hidden id field that can be ampty or contain some

            $content = Array (
                    "status" => $this->input->post("status"),
                    "category_id" => $this->input->post("category_id"),
                    "language" => $this->input->post("language"),
                    "subject" => $this->input->post("subject"),
                    "source" =>  $this->input->post("source"),
                    "author" => $this->input->post("author"),
                    "title" => $this->input->post("title"),
                    "url" => ($this->input->post("url") == "") ? strtolower(url_title(convert_accented_characters($this->input->post("title")))) : $this->input->post("url"), # vea add - validacija da je URL UNIQ-e radi na nivou baze, treba napraviti pristojniju validaciju na nivou aplikacije
                    "image" => $this->input->post("image"), # malo srediti naziv slike, i eventualno kreirati thumb...
                    "date_create" => ($this->input->post('date_create') != '') ? date('Y-m-d H:i:s',strtotime($this->input->post('date_create'))) : date('Y-m-d H:i:s',time()), # if not different / if not empty
                    "date_update" => date('Y-m-d H:i:s',time()), # same as prev...
                    "date_order" => ($this->input->post("date_order") != '') ? $this->input->post("date_order") : '', # if not empty...
                    "introtext" => $this->input->post("introtext"),
                    "fulltext" => str_replace('../../..','',$this->input->post("fulltext")),
                    "tags" => trim($this->input->post("tags")), # if not empty, chunks separated by comma, serialize....
                    "attachments" => serialize(explode("\n",trim($this->input->post("attachments")))), # \n explode pa serialize
                    "gallery" => serialize(explode("\n",trim($this->input->post("gallery")))) # \n explode pa serialize(explode("\n",$this->input->post("gallery")))
            );

            if ($this->input->post('id') == '') {
                $this->db->insert('content', $content);
            } elseif (is_numeric($this->input->post('id'))) {
                $this->db->where('id', $this->input->post('id'));
                $this->db->update('content', $content);
            }

                return true;
    }

    /**
     * [get_content description]
     * @param  string  $id     [description]
     * @param  integer $offset [description]
     * @param  integer $limit  [description]
     * @param  string  $kat    [description]
     * @return Content in an array.
     */
    function get_content($id = 'all',$offset = 0,$limit = 1,$kat = "") {

        switch ($id) { // maknuti switch i postaviti if/else
            case 'all':
                //$this->db->order_by('id','desc');
                $this->db->order_by('date_create','desc');
                $this->db->select('content.*, categories.url as category_url, categories.name as category_name, categories.settings as category_settings');
                if ($kat != "") $this->db->where('category_id',$kat);
                $this->db->join('categories', 'categories.id = content.category_id');
                $query = $this->db->get("content",$limit,$offset);
                $data = $query->result_array();
                break;
            default:
                $this->db->where("id",$id);
                $query = $this->db->get("content");
                $data = $query->row_array();
                break;
        }
            return $data;
    }

    function status_content($id) {
        $new_status = ($this->db->select('status')->from('content')->where('id',$id)->get()->row()->status == 1) ? 0 : 1;
        $data = Array("status" => $new_status);
        $this->db->where('id',$id);
        $insert = $this->db->update('content', $data);
        return true; // vea add - ovaj return nekada nadograditi sa validacijom i false u slucaju greske!!!
    }

    function delete_content($id) {
        $this->db->where('id',$id);
        $query = $this->db->delete('content');
        return true; // vea add - ovaj return nekada nadograditi sa validacijom i false u slucaju greske!!!
    }

/**************************************************************************************************************
************************************************* SETTINGS ***************************************************/

    function get_settings() {

        $data = $this->db->get('settings');

        return $data->result_array();
    }

    function save_settings() {
        $post_data = $this->input->post();

        foreach ($post_data as $key => $value) {
            if ($key != 'submit') {
                if (is_array($value)) {
                    $settings[] =  array( 'key' => $key, 'value' => serialize($value));
                } else {
                    $settings[] =  array( 'key' => $key, 'value' => $value);
                }
            }
        }

        // should be more smart, should have "insert or update" option
        var_dump($settings);die();
        var_dump($post_data);die();

        $this->db->update_batch('settings',$settings,'key');
        return true;
    }

    function save_settings_value($key, $value) {

    }

/***************************************************************************************************************
*************************************************** RESTO *****************************************************/


function _is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect('login');
            die();
        }
    }


}
