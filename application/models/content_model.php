<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_category($url_id) {
        $result = $this->db->where('url',$url_id)->get('categories')->row_array();
        return $result;
    }

    function get_categories() {

        // vea add - ovdje moram da napravim jednu igrariju s obzirom da ovo cudo vraca id unutar podniza a meni bi bilo najzgodnije da je id ustvari kljuc niza
        $tmp = $this->db->get('categories')->result_array();
        $data = array();
            foreach ($tmp as $category) { // ovo treba da bude fja dodata u CI fw u dvije-tri verzije subnode(), subnodes($array,$array,[koji key hocemo]),
                $data[$category['id']] = $category;
            }

        return $data;
    }

    function get_content($category_id,$limit,$offset) {

        $this->db->where('category_id',$category_id);

        $language = $this->session->userdata('language');
        $this->db->where('language',$language);

        $this->db->where('status',1);
//        $this->db->order_by("date_order","desc");  vea add - fix this order!!!
        $this->db->order_by("date_create","desc");
        $result = $this->db->get('content',$limit,$offset);

        return $result->result_array();
    }

    function get_article($post,$cat = '') {

        if (is_numeric($post)) {
            $this->db->where('id',$post);
        } else {  // vea add - this check is not OK!!!
            $this->db->where('url',$post);
            //$language = $this->session->userdata('language');
            //$this->db->where('language',$language);
        }

        if ($cat != '') $this->db->where('category_id',$cat);

        $this->db->where('status',1);
        $result = $this->db->get('content');
//        var_dump($result->result_arrau());
        return $result->result_array();
    }

    function get_sidebar_links($category_id = "", $limit = "") {

        $this->db->select('id, url, title, ');
        $this->db->where('category_id',$category_id);
        $this->db->where('status',1);
        $this->db->order_by("date_order","desc");
        $this->db->order_by("date_create","desc");
        $result = $this->db->get('content',$limit);

        return $result->result_array();
    }

    function get_sidebar_content($category_id = "") {

        $ids = $this->db->where('id',$category_id)->get('categories')->row()->sidebar_content;
//        var_dump($ids);die();
        $this->db->flush_cache();

        $this->db->where_in('id',explode(',',$ids));
        $this->db->where('status',1);
//        $this->db->where('category_id',5); // vea add - TODO - izbaciti ovo kad se uvede sistem za konfiguraciju kategorija itd...
        $this->db->order_by("date_order","desc");
        $this->db->order_by("date_create","desc");
        $result = $this->db->get('content');
//        var_dump($result->result_array()); die();

        return $result->result_array();
    }


//    function get_posts_by_category($key = 'ALL', $offset = '', $limit = '') {
//        $key = TRUE;
//        $offset = TRUE;
//        $limit = TRUE;
//    }
//
//    function get_post_by_id($id = '') {
//
//    }
//
//    function sitemap($id = '') {
//
//    }

}