<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('check_captcha'))
{
    function check_captcha()
    {
        $CI =& get_instance();

        $expiration = time()-1800;
	$CI->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);

	$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
	$binds = array($_POST['captcha'], $CI->input->ip_address(), $expiration);
	$query = $CI->db->query($sql, $binds);
	$row = $query->row();

	if ($row->count == 0)
	{
                      $CI->form_validation->set_message('check_captcha', 'Morate unjeti tačan broj sa slike!');
                      return false;
	} else {
            return true;
        }
    }
}


if ( ! function_exists('create_captcha_data'))
{
    function create_captcha_data()
    {
        $CI =& get_instance();

        $CI->load->helper('captcha');
        $val = array(
                'word'       => strtoupper(random_string('nozero',4)),
                'img_path'	 => 'images/captcha/',
                'img_url'     => base_url().'images/captcha/',
                'font_path'	 => 'system/fonts/RSTOULOU.TTF',
                'img_width'	 => '150',
                'img_height' => '40',
                'expiration' => 7200
        );

        $cap = create_captcha($val);

        $captcha = array(
            'captcha_id'	=> '',
            'captcha_time'	=> $cap['time'],
            'ip_address'	=> $CI->input->ip_address(),
            'word'	=> $cap['word']
        );

        $query = $CI->db->insert_string('captcha', $captcha);
        $CI->db->query($query);

        return $cap;
    }
}
