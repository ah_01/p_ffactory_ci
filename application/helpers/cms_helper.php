<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('parse_emails')) {
	function parse_emails($string, $file = null)
	{
				/////////////////// email parser
		
		//Check if file is defined then if it is read it to string else string is  textual string sent
		if ($file != null) {
			
			$string = file_get_contents($file); // Load text file contents
		}

		// don't need to preassign $matches, it's created dynamically
		// this regex handles more email address formats like a+b@google.com.sg, and the i makes it case insensitive
		$pattern = '/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';

		// preg_match_all returns an associative array
		preg_match_all($pattern, $string, $matches);

		// the data you want is in $matches[0], dump it with var_export() to see it
		return($matches[0]);
		// var_dump($matches[0]);
	}
}

	if (!function_exists('alert_m')) {

		//$type  = alert-danger, alert-info, alert-success, alert-warning - BOOTSRAP 
		
	function alert_m($message, $type = 'alert-danger' )
	{


	if ($message != FALSE) {
		$end = '';
		if (strlen($message)>313) {
			$message = substr($message, 1, 313);
		$end .= ' ...;';
		}

		$div = '<div class="alert '.$type.'" role="alert">';
		$div.= $message;
		// var_dump($message);
		$div .= $end;
		$div .= '</div>';

	}else{
		$div = NULL;
	}

	echo $div;	

	}
}

