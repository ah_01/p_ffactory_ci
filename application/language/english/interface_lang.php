<?php

// SSST

$lang['date'] = "Date";
$lang['more'] = "more";
$lang['news'] = "News";
$lang['events'] = "Events";
$lang['media_library'] = "Media Library";
$lang['read_more'] = "read more";
$lang['search_no_data_message'] = "No data...";
$lang['search_term'] = "Search term";
$lang['search_results_number'] = "Results";

// English Language File

$lang['read_more'] = "read more";
$lang['latest_news'] = "Latest news";
$lang['all_rights_reserver'] = "All rights reserved.";
$lang['contact_us'] = "Contact Us";
$lang['related'] = "Related";

$lang['homepage'] = "Home";

$lang['about_us'] = "About Us";
$lang['board'] = "Board";
$lang['documents'] = "Documents";
$lang['management'] = "Management";
$lang['goals'] = "Goals";
$lang['message'] = "Message";

$lang['projects_and_investments'] = "Projects & Investments";
$lang['project1'] = "New Project";
$lang['project2'] = "Project Ilijaš";
$lang['project3'] = "Business and residential complex Otes";
$lang['project4'] = "The Belvedere project";
$lang['project5'] = "Project construction of the villas complex in Ilijaš";
$lang['project6'] = "Villa Ilidža project";
$lang['project7'] = "Project 7";
$lang['project8'] = "Project 8";

$lang['news'] = "News";
$lang['info_bih'] = "Info BiH";
$lang['contact'] = "Contact";

$lang['text_front_intro_title'] = "Compact Invest Company";
$lang['text_front_intro_text'] = "<p>Beginnings of Compact Invest Company rely upon expertise and global historical real estate experience for more than half century prior to  entering Bosnia and  Herzegovina. Compact Invest Company, after Allah, relies upon its reputation, credibility, commitment to work and a serious approach to work. <br /><br />Therefore we want to offer special and the best development projects through services improvement and content improvement, so the services and the content can prove the world's best technical features and standards. Compact Invest uses every investment opportunity through which some of the Company's goals can be realized, such as: development, economic growth, making profits and welfare of BiH society as well as the tourists. At Compact invest Company, we strive to bring something new and innovative. We work hard and stand up for You. We invest, innovate and work hard for You. </p>";
$lang['text_dont_hesitate'] = "Don't hesitate to send us a message!";

// Categorie Keys and Descriptions

$lang['cat_title_projects-investments'] = "Projects & Investments";
$lang['cat_text_projects-investments'] = "Projects & Investments description...";
$lang['cat_title_about-us'] = "About Us";
$lang['cat_text_about-us'] = "About Us description...";
$lang['cat_title_bih-info'] = "Info BiH";
$lang['cat_text_bih-info'] = "Info BiH description...";
$lang['cat_title_news'] = "News";
$lang['cat_text_news'] = "News description....";
$lang['cat_title_contact'] = "Contact";
$lang['cat_text_contact'] = "Don't hesitate to send us a message!";

// header wrapper content based on category key

$lang['header_wrapper_title'] = "Our company's blog";
$lang['header_wrapper_message'] = "Innova Construct proudly presents it’s blog! Here we share our passion with our readers of constructing majestic tunnels, buildings, gardens and a lot more!";

// Contact page

$lang['contact_introtext'] = "Short intro text for contact page...";
$lang['contact_title_main'] = "Send us a message";
$lang['contact_label_name'] = "Your Name";
$lang['contact_label_email'] = "Email";
$lang['contact_label_subject'] = "Subject";
$lang['contact_label_message'] = "Your Message";
$lang['contact_label_captcha'] = "Numbers from image";
$lang['contact_label_send'] = "send message";
$lang['contact_fail_message'] = "Please check if you've filled all the fields with valid information. Thank you.";
$lang['contact_success_message'] = "<strong>Your message has been sent!</strong><br />Thank you for emailing us! We will get in touch with you soon.";


$lang['contact_title_map'] = "Our location";
$lang['contact_title_info'] = "Contact info";
$lang['contact_details'] = "<li>Compact Invest d.o.o.</li><li>Mustafe Pintola 31<br />71210 Ilidža</li>
<li>Phone:  +387 33 123 456<br />Fax: +387 33 123 456</li>
<li>Website: <a href=\"http://www.compactinvest.ba\">http://www.compactinvest.ba</a><br />
Email: <a href=\"mailto:info@compactinvest.ba\">info@compactinvest.ba</a></li>";

// URLS for Menu

$lang['url_project1'] = "104";
$lang['url_project2'] = "105";
$lang['url_project3'] = "106";
$lang['url_project4'] = "131";
$lang['url_project5'] = "132";
$lang['url_project6'] = "133";
$lang['url_project7'] = "118";
$lang['url_project8'] = "118";

$lang['url_board'] = "96";
$lang['url_documents'] = "97";
$lang['url_management'] = "98";
$lang['url_goals'] = "99";
$lang['url_message'] = "100";


?>