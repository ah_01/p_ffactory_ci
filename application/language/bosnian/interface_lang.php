<?php

// SSST

$lang['date'] = "Datum";
$lang['more'] = "Opširnije";
$lang['news'] = "Novosti";
$lang['events'] = "Događaji";
$lang['media_library'] = "Media Library";
$lang['read_more'] = "čitaj više";
$lang['search_no_data_message'] = "Nema podataka...";
$lang['search_term'] = "Pojam za pretragu";
$lang['search_results_number'] = "Rezultata";

// Bosnian Language File


$lang['read_more'] = "čitaj više";
$lang['latest_news'] = "Poslednje novosti";
$lang['all_rights_reserver'] = "Sve prava pridržana.";
$lang['contact_us'] = "Kontakt";
$lang['related'] = "Povezano";

$lang['homepage'] = "Home";

$lang['about_us'] = "O nama";
$lang['board'] = "Upravni odbor";
$lang['documents'] = "Dokumenti";
$lang['management'] = "Menadžment";
$lang['goals'] = "Ciljevi";
$lang['message'] = "Poruka";

$lang['projects_and_investments'] = "Projekti i Investicije";
$lang['project1'] = "Villa Ilidža project";
$lang['project2'] = "Projekt Ilijaš";
$lang['project3'] = "Stambeni-poslovni kompleks Otes";
$lang['project4'] = "Projekt Vidikovac";
$lang['project5'] = "Projekt izgradnje kompleks vila u Ilijašu";
$lang['project6'] = "Projekt Vila Ilidža";
$lang['project7'] = "Project 7";
$lang['project8'] = "Project 8";

$lang['news'] = "Novosti";
$lang['info_bih'] = "Info BiH";
$lang['contact'] = "Kontakt";

$lang['text_front_intro_title'] = "Compact Invest Company";
$lang['text_front_intro_text'] = "<p>Začeci kompanije Compact Invest se oslanjaju na stručnost i povijesno svjetsko iskustvo u nekretninama koje seže više od pola stoljeća prije samog dolaska u BiH. Compact Invest se, nakon Uzvišenog Allaha, oslanja na svoj ugled, kredibilitet, posvećenost radu kao i ozbiljnom pristupu radu. Time želimo ponuditi ono što je posebno i najbolje od razvojnih projekata, a sve kroz jačanje usluga i sadržaja koji će pri realizaciji istih pokazati najkvalitetnije tehničke i svjetske karakteristike i standarde. <br /><br /> Compact Invest koristi svaku investicijsku priliku kojom se može ostvariti neki od ciljeva kompanije: razvoj, ekonomski napredak, ostvarenje profita i blagostanje kako bh. društva tako i turista. Mi u Compact Investu nastojimo da donesemo nešto novo i inovativno. Naporno radimo i zalažemo se za Vas.</p>";
$lang['text_dont_hesitate'] = "Nemoj te se ustručavati poslati poruku!";

// Categorie Keys and Descriptions

$lang['cat_title_projects-investments'] = "Projekti i Investicije";
$lang['cat_text_projects-investments'] = "Projekti i Investicije opis...";
$lang['cat_title_about-us'] = "About Us";
$lang['cat_text_about-us'] = "About Us description...";
$lang['cat_title_bih-info'] = "Info BiH";
$lang['cat_text_bih-info'] = "Info BiH description...";
$lang['cat_text_news'] = "News description....";
$lang['cat_title_contact'] = "Kontakt";
$lang['cat_text_contact'] = "Nemoj te se ustručavati poslati poruku!";

// Contact page

$lang['contact_introtext'] = "Kratki uvodni text....";
$lang['contact_title_main'] = "Pošalji te nam poruku";
$lang['contact_label_name'] = "Vaše Ime";
$lang['contact_label_email'] = "Email";
$lang['contact_label_subject'] = "Predmet";
$lang['contact_label_message'] = "Poruka";
$lang['contact_label_captcha'] = "Broj sa slike";
$lang['contact_label_send'] = "pošalji poruku";
$lang['contact_fail_message'] = "Molimo provjeri te podatke koje ste unjeli. Sva polja su obavezna.";
$lang['contact_success_message'] = "<strong>Vaša poruka je poslana!</strong><br />Hvala što ste nas kontaktirali, očekuj te uskoro naš odgovor.";

$lang['contact_title_map'] = "Naša lokacija";
$lang['contact_title_info'] = "Kontakt info";
$lang['contact_details'] = "<li>Compact Invest d.o.o.</li><li>Mustafe Pintola 31<br />71210 Ilidža</li>
<li>Phone:  +387 33 123 456<br />Fax: +387 33 123 456</li>
<li>Website: <a href=\"http://www.compactinvest.ba\">http://www.compactinvest.ba</a><br />
Email: <a href=\"mailto:info@compactinvest.ba\">info@compactinvest.ba</a></li>";

// URLS for Menu

$lang['url_project1'] = "116";
$lang['url_project2'] = "117";
$lang['url_project3'] = "118";
$lang['url_project4'] = "134";
$lang['url_project5'] = "135";
$lang['url_project6'] = "136";
$lang['url_project7'] = "118";
$lang['url_project8'] = "118";


$lang['url_board'] = "108";
$lang['url_documents'] = "109";
$lang['url_management'] = "110";
$lang['url_goals'] = "111";
$lang['url_message'] = "112";

?>