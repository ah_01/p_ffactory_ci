<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Emails_table_addFIelds extends CI_Migration {

	public function up()
	{
		$fields = array(
			'created' => array(
				'type' => 'TIMESTAMP',
				'null' => FALSE, 
				'value'=> 'CURRENT_TIMESTAMP'
			),
			'updated' => array(
				'type' => 'TIMESTAMP',
				'value'=> 'ON UPDATE CURRENT_TIMESTAMP'  //CHECK this one ?
			)
		);

		$this->dbforge->add_column('emails', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('emails', $fields);
	}

}