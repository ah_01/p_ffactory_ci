<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_letters_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'subject' => array(
				'type' => 'VARCHAR',
				'constraint' => '41',
			),
			'body' => array(
				'type' => 'TEXT',
				'constraint' => '41',
			), 
			'test_email' => array(
				'type' => 'VARCHAR',
				'constraint' => '41',
				'value'	=> NULL
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('letters');
	}

	public function down()
	{
		$this->dbforge->drop_table('letters');
	}

}