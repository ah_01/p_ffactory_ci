<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Emails_table_addFields extends CI_Migration {

	public function up()
	{
		$fields = array(
			'mgroup' => array(
				'type' => 'VARCHAR',
				'null' => TRUE, 
				'constraint'=> '11'
			),
			
		);

		$this->dbforge->add_column('emails', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('emails', $fields);
	}

}