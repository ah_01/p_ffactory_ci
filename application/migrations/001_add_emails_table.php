<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_emails_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '41',
			),
			'status' => array(
				'type' => 'BOOLEAN',
				'value' => FALSE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('emails');
	}

	public function down()
	{
		$this->dbforge->drop_table('emails');
	}

}