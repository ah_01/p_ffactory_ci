<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Letters_table_addFIelds extends CI_Migration {

	public function up()
	{
		$fields = array(
			'status' => array(
				'type' => 'BOOLEAN',
				'value'=> FALSE
			),
		
		);

		$this->dbforge->add_column('letters', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('letters', $fields);
	}

}