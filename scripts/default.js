/* Author: AdvanceIT */
jQuery(document).ready(function($){
    $("#post-gallery").PikaChoose({
            autoPlay: false,
            text: { play: "Play", stop: "Stop", previous: "Back", next: "Next", loading: "Loading" },
            carousel:false,
            carouselOptions:{wrap:'circular'},
            showCaption:true,
            showTooltips: false
    });
});
