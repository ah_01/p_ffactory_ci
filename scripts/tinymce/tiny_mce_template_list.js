var tinyMCETemplateList = new Array(
        // Name, URL
        ["ssst-2col-images", "/scripts/tinymce/templates/ssst-2col-images.html"],
        ["ssst-2col-left-image", "/scripts/tinymce/templates/ssst-2col-left-image.html"],
        ["ssst-2col-right-image", "/scripts/tinymce/templates/ssst-2col-right-image.html"],
        ["ssst-gray-bg-with-image", "/scripts/tinymce/templates/gray-bg-with-image.html"],
        ["ssst-full-width-image", "/scripts/tinymce/templates/ssst-full-width-image.html"]
);
