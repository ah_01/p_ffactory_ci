	$.ajaxSetup ({
		cache: false
	});
	var ajax_load = "<img class='loading' src='img/load.gif' alt='loading...' />";
	
//	load() functions
	var loadUrl = "ajax/load.php";
	$("#load_basic").click(function(){
		$("#result").html(ajax_load).load(loadUrl);
	});
	
	$("#load_dom").click(function(){
		$("#result")
			.html(ajax_load)
			.load(loadUrl + " #picture");
	});
	
	$("#load_get").click(function(){
		$("#result")
			.html(ajax_load)
			.load(loadUrl, "language=php&version=5");
	});
	
	$("#load_post").click(function(){
		$("#result")
			.html(ajax_load)
			.load(loadUrl, {language: "php", version: 5});
	});
	
	$("#load_callback").click(function(){
		$("#result")
			.html(ajax_load)
			.load(loadUrl, null, function(responseText){
				alert("Response:\n" + responseText);
			});
	});
	
//	$.getJSON()
	var jsonUrl = "ajax/json.php";
	$("#getJSONForm").submit(function(){
		var q = $("#q").val();
		if (q.length == 0) {
			$("#q").focus();
		} else {
			$("#result").html(ajax_load);
			$.getJSON(
				jsonUrl,
//				"q="+q,
				{q: q},
				function(json) {
					var result = "<p>Language code is \"<strong>" + json.responseData.language + "</strong>\"</p>";
					$("#result").html(result);
				}
			);
		}
		return false;
	});
	
//	$.getScript()
	var scriptUrl = "ajax/script.php";
	$("#getScript").click(function(){
		$("#result").html(ajax_load);
		$.getScript(scriptUrl, function(){
			$("#result").html("");
		});
	});

//	$.get()
	$("#get").click(function(){
		$("#result").html(ajax_load);
		$.get(
			loadUrl,
			{language: "php", version: 5},
			function(responseText){
				$("#result").html(responseText);
			},
			"html"
		);
	});
	
//	$.post()
	$("#btn").click(function(){
		$("#results").html(ajax_load);
		$.post(
			loadUrl,
			{language: "php", version: 5},
			function(responseText){
				$("#result").html(responseText);
			},
			"html"
		);
	});
