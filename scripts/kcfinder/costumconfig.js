CKEDITOR.editorConfig = function(config) {
// ...
   config.filebrowserBrowseUrl = 'http://ff.app/scripts/kcfinder/browse.php?opener=ckeditor&type=files';
   config.filebrowserImageBrowseUrl = 'http://ff.app/scripts/kcfinder/browse.php?opener=ckeditor&type=images';
   config.filebrowserFlashBrowseUrl = 'http://ff.app/scripts/kcfinder/browse.php?opener=ckeditor&type=flash';
   config.filebrowserUploadUrl = 'http://ff.app/scripts/kcfinder/upload.php?opener=ckeditor&type=files';
   config.filebrowserImageUploadUrl = 'http://ff.app/scripts/kcfinder/upload.php?opener=ckeditor&type=images';
   config.filebrowserFlashUploadUrl = 'http://ff.app/scripts/kcfinder/upload.php?opener=ckeditor&type=flash';
// ...
};